from diagrams import Diagram, Cluster, Node
from diagrams.aws.compute import EC2Instance as singleJob
from diagrams.aws.compute import EC2Instances as multiJob
from diagrams.aws.management import OpsworksPermissions as locks
from diagrams.aws.iot import IotAnalyticsDataSet as file

with Diagram("Worflow: phisym", filename="phisym-wflow", show=False, direction="TB") as d:
    # s = singleJob("Single job", width='0.5')
    # m = multiJob("Batch jobs", width='0.5')
    # f = file("Workflow related files", width='0.5')
    with Cluster("phisym-reco", direction="LR"):
        Node("", width='1.', height='0', style='invisible')
        t0_lock = locks("T0ProcDatasetLock \n dataset=/AlCaPhiSym \n stage=Repack", width='1.6')
        gt_lock = locks("CondDBLockGT: \n EcalLaserAPDPNRatiosRcd \n  EcalPedestalsRcd", width='1.6')
        reco_files = file("runreco.py \n alcanano_customize.py \n template.sub \n batch_script.sh")
        phisym_reco = multiJob("HTCHandlerByRunDBS \n dsetname=/AlCaPhiSym/*/RAW")

    [t0_lock, gt_lock] >> phisym_reco
