#!/usr/bin/env python3
import sys
import subprocess
from os import path
from typing import Optional, List
from ecalautoctrl import JobCtrl, RunCtrl, HTCHandler, process_by_intlumi, prev_task_data_source
from ecalautoctrl.TaskHandlers import AutoCtrlScriptBase

@prev_task_data_source
@process_by_intlumi(target=2000)
class HTCHandlerAlignment(HTCHandler):
    """
    Alignment submission handler. Submit 1 job per SM/Dee reading data from
    the previous reconstruction step.
    
    :param task: task name.
    :param prev_input: name of the task from which gather the input data.
    :param deps_tasks: list of workfow dependencies.
    """

    def __init__(self,
                 task: str,
                 prev_input: str,
                 **kwargs):
        super().__init__(task=task, deps_tasks=[prev_input], **kwargs)
        self.prev_input = prev_input
    
    def submit(self):
        """
        Submit runs in status task: new.
        Resubmit runs in status task: reprocess.
        """
        for group in self.groups():
            # master run
            run = group[-1]
            fdict = self.get_files(group)
            if fdict is not None and len(fdict)>0:
                fdict = [f'file:{f}' for f in fdict]
                jctrl = JobCtrl(task=self.task,
                                campaign=self.campaign,
                                tags={'run_number': run['run_number'], 'fill': run['fill']},
                                dbname=self.opts.dbname)
                # allow reprecessing only if previous jobs are not running
                allow_repr = (run[self.task] == 'reprocess')
                if jctrl.taskExist() and run[self.task] == 'reprocess':
                    jobs = jctrl.getJobs()
                    if len(jobs['idle']+jobs['running']) > 0:
                        self.log.info(f"Reprocessing of run {run['run_number']} from fill {run['fill']} not allowed because {len(jobs['idle']+jobs['running'])} job(s) are in 'idle' or 'running' state.")
                        allow_repr = False
                if not jctrl.taskExist() or allow_repr:
                    task = f'-w {self.task} -c {self.campaign} -t run_number:{run["run_number"]},fill:{run["fill"]} --db {self.opts.dbname}'
                    eosdir = path.abspath(self.opts.eosdir+f'/{run["run_number"]}/')
                    # split processing such that each job runs on 1 SM/Dee
                    # 0->EB (36 SM, from 0 to 35)
                    parts = [(0, i) for i in range(36)]
                    # 1->EE (4 Dees, from 1 to 4)                        
                    parts.extend([(1, i) for i in range(4)])
                    with open('args-align.txt', 'w') as ff:
                        ff.write('\n'.join([f'{p}, {sp}' for p,sp in parts]))
                    infiles = ','.join(fdict)
                    ret = subprocess.run(['condor_submit',
                                          f'{self.opts.template}',
                                          '-spool',
                                          '-queue', 'part, subpart from args-align.txt',
                                          '-append', f'arguments = "$(ClusterId).$(ProcId) $(ProcId) {infiles} \'{task}\' {eosdir} {self.opts.wdir} $(part) $(subpart)"',
                                          '-append', f'output = /eos/home-e/ecalgit/www/logs/{self.task}-$(ClusterId)-$(part)-$(subpart).log',
                                          '-append', f'error = /eos/home-e/ecalgit/www/logs/{self.task}-$(ClusterId)-$(part)-$(subpart).log'],
                                         capture_output=True)
                    #remove('args.txt')
                    if ret.returncode == 0:
                        # awfully parse condor_submit output to get the clusterId
                        self.log.info(f'Submitting jobs for run(s): '+','.join([r['run_number'] for r in group])+f' fill {run["fill"]}')
                        self.log.info(ret.stdout.decode().strip())
                        cluster = ret.stdout.decode().strip().split()[-1][:-1]
                        logurl = 'https://ecallogs.web.cern.ch/'
                        # create task injecting further job info:
                        # - input files
                        # - group: other runs processed by this task
                        # - part: (detector region, SM/Dee) tuple
                        # - htc-id: HTCondor job id
                        # - log file
                        jctrl.createTask(jids=list(range(len(parts))),
                                         recreate=allow_repr,
                                         fields=[{'group': ','.join([r['run_number'] for r in group[:-1]]),
                                                  'inputs': infiles,
                                                  'part': ','.join([str(p), str(sp)]),
                                                  'htc-id': f"{cluster}.{i}",
                                                  'log': f'{logurl}/{self.task}-{cluster}-{p}-{sp}.log'} for i,(p,sp) in enumerate(parts)])
                        # set the master run to status processing
                        self.rctrl.updateStatus(run=run['run_number'], status={self.task: 'processing'})
                        if len(group) > 1:
                            # set status merged for all other runs.
                            for r in group[:-1]:
                                self.rctrl.updateStatus(run=r['run_number'], status={self.task: 'merged'})
                    else:
                        self.log.info("failed to submit jobs to condor. Error msg:"+ret.stderr.decode())
                        return -1
        return 0

    def resubmit(self):
        """
        Query for failed jobs and resubmit.
        Note: condor logs are overwritten.
        """

        runs = self.rctrl.getRuns(status={self.task: 'processing'})

        for run_dict in runs:
            run = run_dict['run_number']
            jctrl = JobCtrl(task=self.task,
                            campaign=self.campaign,
                            tags={'run_number': run, 'fill': run_dict['fill']},
                            dbname=self.opts.dbname)
            # check for evicted jobs. Jobs still marked as running in the db but not
            # actually running in condor.
            for jid in jctrl.getRunning():
                if not self.check_running_job(jid = jctrl.getJob(jid=jid, last=True)[-1]['htc-id']):
                    jctrl.failed(jid=jid)

            # resubmit failed
            jobs = jctrl.getJobs()
            failedjobs = jobs['failed']
            n_idlejobs = len(jobs['idle'])
            n_runningjobs = len(jobs['running'])
            n_donejobs = len(jobs['done'])
            n_jobs = len(failedjobs) + n_idlejobs + n_runningjobs + n_donejobs
            if not jctrl.taskExist() or failedjobs == []:
                self.log.info(f'No failures found for run {run} with {n_jobs} job(s). Job status: {n_idlejobs} idle, {n_runningjobs} running, {n_donejobs} done, {len(failedjobs)} failed.')
            else:
                self.log.info(f'Found {len(failedjobs)} failed job(s) for run {run} with {n_jobs} job(s). Job status: {n_idlejobs} idle, {n_runningjobs} running, {n_donejobs} done, {len(failedjobs)} failed.')
                # Building the resubmission arguments
                # jobid, files
                # where jobid is the jobid of the original submission
                eosdir = path.abspath(self.opts.eosdir+f'/{run}/')
                flist = jctrl.getJob(0, last=True)[0]['inputs']
                task = f'-w {self.task} -c {self.campaign} -t run_number:{run},fill:{run_dict["fill"]} --db {self.opts.dbname}'
                parts = [str(i)+', '+jctrl.getJob(i, last=True)[0]['part'] for i in failedjobs]
                with open('args-align.txt', 'w') as ff:
                    ff.write('\n'.join(parts))

                ret = subprocess.run(['condor_submit',
                                      f'{self.opts.template}',
                                      '-spool',
                                      '-queue', 'jobid, part, subpart from args-align.txt',
                                      '-append', f'arguments = "$(ClusterId).$(ProcId) $(jobid) {flist} \'{task}\' {eosdir} {self.opts.wdir} $(part) $(subpart)"',
                                      '-append', f'output = /eos/home-e/ecalgit/www/logs/{self.task}-$(ClusterId)-$(part)-$(subpart).log',
                                      '-append', f'error = /eos/home-e/ecalgit/www/logs/{self.task}-$(ClusterId)-$(part)-$(subpart).log',
                                      '-append', f'+JobFlavour = "{self.opts.resubflv}"'],
                                     capture_output=True)
                #remove('args.txt')
                if ret.returncode == 0:
                    # awfully parse condor_submit output to get the clusterId
                    self.log.info(ret.stdout.decode().strip())
                    cluster = ret.stdout.decode().strip().split()[-1][:-1]
                    logurl = 'https://ecallogs.web.cern.ch/'
                    # set the job status to idle and update the htc-id and log file URL
                    for i, jid in enumerate(failedjobs):
                        part = jctrl.getJob(jid, last=True)[0]['part']
                        p = part.split(',')[0]
                        sp = part.split(',')[1]
                        jctrl.idle(jid=jid, fields={'htc-id': f"{cluster}.{i}", 'log': f'{logurl}/{self.task}-{cluster}-{p}-{sp}.log'})
                else:
                    self.log.info("failed to submit jobs to condor. Error msg:"+ret.stderr.decode())
                    return -1
        return 0

    
if __name__ == '__main__':
    handler = HTCHandlerAlignment(task='alignment-align',
                                  prev_input='alignment-reco')

    ret = handler()

    sys.exit(ret)

get_opts = AutoCtrlScriptBase.export_options(HTCHandlerAlignment)
    
