#!/bin/bash

set -x

CLUSTERID=${1}
JOBID=${2}
INFILE=${3}
TASK=${4}
EOSDIR=${5}
WDIR=${6}

trap 'echo "Kill signal received"; ecalautomation.py $TASK jobctrl --id $JOBID --failed; exit' SIGKILL SIGTERM

export HOME=/afs/cern.ch/user/e/ecalgit/
export EOS_MGM_URL=root://eoscms.cern.ch

kinit ecalgit -k -t ecalgit.keytab

cd $WDIR
eval $(scram runtime -sh)
cd -

ecalautomation.py $TASK jobctrl --id $JOBID --running --fields "htc-id:${CLUSTERID}"

# Execute the actual processing here, this can be a CMSSW job or anything else (cmsRun is just an example) 
cmsRun bla.py
RETCMSSW=$?

if [ "$RETCMSSW" == "0" ]
then
    # move the file to the final location (example)
    eos mkdir -p $EOSDIR
    LIST=$(ls output/*root)
    OFILE=""
    RETCOPY=0
    for file in ${LIST}; do
        xrdcp -f ${file} $EOS_MGM_URL/$EOSDIR/
        ((RETCOPY=$?))
        OFILE="${OFILE},${EOSDIR}/"`basename ${file}`
    done
    OFILE="${OFILE:1}"
else
    RETCOPY=1
fi

RET=$(echo "$RETCMSSW+$RETCOPY" | bc)

if [ "$RET" == "0" ]
then
    ecalautomation.py $TASK jobctrl --id $JOBID --done --fields "output:${OFILE}"
else
    ecalautomation.py $TASK jobctrl --id $JOBID --failed
fi

exit $RET
