#!/bin/bash

set -x

CLUSTERID=${1}
JOBID=${2}
INFILE=${3}
TASK=${4}
EOSDIR=${5}
WDIR=${6}
GT=${7}
CERTJSON=${8}

trap 'echo "Kill signal received"; ecalautomation.py $TASK jobctrl --id $JOBID --failed; exit' SIGKILL SIGTERM

export HOME=/afs/cern.ch/user/e/ecalgit/
export EOS_MGM_URL=root://eoscms.cern.ch

source /cvmfs/cms.cern.ch/cmsset_default.sh
cd $WDIR
eval $(scram runtime -sh)
cd -

ecalautomation.py $TASK jobctrl --id $JOBID --running --fields "htc-id:${CLUSTERID}"

mkdir output/

if [ -z "$CERTJSON" ]; then
  LUMITOPROC=""
else
  LUMITOPROC="--lumiToProcess=$CERTJSON"
fi

export PYTHON3PATH=$PYTHON3PATH:$(pwd)
export PYTHONPATH=$PYTHONPATH:$(pwd)
cmsDriver.py -s RECO:bunchSpacingProducer+ecalMultiFitUncalibRecHitTask+ecalCalibratedRecHitTask,ALCA:EcalPhiSymByRun \
             --conditions ${GT} --era Run3 \
             --eventcontent=ALCARECO --datatier ALCARECO -n -1 --no_exec \
             --python_filename=EcalPhiSymReco_cfg.py \
             --nThreads=2 --nStreams=2 --nConcurrentLumis=2 \
             --customise=alcanano_customize.py \
             --customise_commands='process.MessageLogger.cerr.FwkSummary.reportEvery = 100000 \n process.MessageLogger.cerr.FwkReport.reportEvery = 100000' \
             --filein=$INFILE $LUMITOPROC

cmsRun -n 2 EcalPhiSymReco_cfg.py
RETCMSSW=$?

if [ "$RETCMSSW" == "0" ]
then
    eos mkdir -p $EOSDIR
    xrdcp -f ecal_phisym_nano.root $EOS_MGM_URL/$EOSDIR/phisymreco_nano_$JOBID.root
    RETCOPY=$?
    rm *root
    OFILE=$EOSDIR/phisymreco_nano_$JOBID.root
else
    RETCOPY=1
fi

RET=$(echo "$RETCMSSW+$RETCOPY" | bc)

if [ "$RET" == "0" ]
then
    ecalautomation.py $TASK jobctrl --id $JOBID --done --fields "output:${OFILE}"
else
    ecalautomation.py $TASK jobctrl --id $JOBID --failed
fi

exit $RET
