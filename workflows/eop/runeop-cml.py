#!/usr/bin/env python3
from copy import deepcopy
import os
import sys
from typing import Dict
from typing import Optional, List, Tuple
from ecalautoctrl import JobCtrl, HandlerBase
from ecalautoctrl.TaskHandlers import AutoCtrlScriptBase
from job_ctrl_helpers import (
    prev_task_data_source,
    process_by_intlumi,
    same_year,
)

import glob
import json
import pickle
from datetime import datetime
import matplotlib as mpl

mpl.use("Agg")
import matplotlib.pyplot as plt
import matplotlib.dates as md
import numpy as np
import traceback as tb

import concurrent.futures

import ROOT

ROOT.gROOT.SetBatch(1)
ROOT.gErrorIgnoreLevel = ROOT.kWarning  # switch off 'Info in <TCanvas::Print>: blabla'


@prev_task_data_source
@process_by_intlumi(target=2000, group_filter=same_year)
class EopCmlHandler(HandlerBase):
    """
    Execute all the steps to generate the eop monitoring plots.
    Process fills that have been dumped (completed).

    :param task: task name.
    :param prev_input: name of the task from which gather the input data.
    :param deps_tasks: list of workfow dependencies.
    """

    def __init__(
        self,
        task: str,
        prev_input: str,
        deps_tasks: Optional[List[str]] = None,
        **kwargs,
    ):
        super().__init__(task=task, deps_tasks=deps_tasks, **kwargs)
        self.prev_input = prev_input

        self.submit_parser.add_argument(
            "--plotsdir",
            dest="plotsdir",
            default=None,
            type=str,
            help="Base path of plot location on EOS",
        )

        self.submit_parser.add_argument(
            "--plotsurl",
            dest="plotsurl",
            default=None,
            type=str,
            help="Base url for plots",
        )

        self.resubmit_parser.add_argument(
            "--plotsdir",
            dest="plotsdir",
            default=None,
            type=str,
            help="Base path of plot location on EOS",
        )

        self.resubmit_parser.add_argument(
            "--plotsurl",
            dest="plotsurl",
            default=None,
            type=str,
            help="Base url for plots",
        )

    @staticmethod
    def HarnessLimits(harnessname):
        if "/" in harnessname:
            for token in harnessname.split("/"):
                if "IEta" in token:
                    return EopCmlHandler.HarnessLimits(token)

        return (
            int(harnessname.split("_")[1]),
            int(harnessname.split("_")[2]),
            int(harnessname.split("_")[4]),
            int(harnessname.split("_")[5]),
        )

    @staticmethod
    def writeIC(icfilename: str = None, ICmap: Dict = None):
        """
        Write a set of IC from a single IOV into a txt file. EB only

        Output format: ix iy iz IC IC error

        :param icfilename: output file name.
        :param ICmap: dictionary (ieta, iphi) containing the IC values
        """
        with open(icfilename, "w") as icfile:
            for ieta, iphi_ICmap in ICmap.items():
                for iphi, IC in iphi_ICmap.items():
                    icfile.write("%i\t%i\t0\t%f\t%f\n" % (ieta, iphi, *IC))

    @staticmethod
    def format_harness(harness):
        values = harness.split("_")
        return "$ {} < i\eta < {} \;\;\;  {} < i\phi < {} $ ".format(
            values[1], values[2], values[4], values[5]
        )

    @staticmethod
    def get_runs(basepath):
        files = glob.glob(f"{basepath}/*")
        folders = [k.split("/")[-1] for k in files if os.path.isdir(k)]
        runs = sorted([int(k) for k in folders if k.isdigit()])
        return runs

    def strip_path(self, path: str) -> str:
        s = path.split("/")
        return "/".join([""] + list(filter(lambda k: k != "", s)))

    @staticmethod
    def normalize(eospath, plotsdir):
        runs = EopCmlHandler.get_runs(eospath)
        norm = 0
        norm_year = 0

        xs = []
        x_errs = []
        ys = {}
        y_errs = {}

        for run in runs:
            with open(f"{eospath}/{run}/results.pkl", "rb") as file:
                results = pickle.load(file)

            year = datetime.fromtimestamp(results["IOV_info"]["time_begin"]).year

            if isinstance(norm, int) or norm_year != year:
                print("change norm", year, run)
                norm = deepcopy(results)
                norm_year = year

            IOV = [
                results["IOV_info"]["runs_begin"],
                results["IOV_info"]["lumisection_begin"],
                results["IOV_info"]["runs_end"],
                results["IOV_info"]["lumisection_end"],
            ]
            IOV = list(map(lambda k: int(k), IOV))
            IC_name = f"IC_{IOV[0]}-{IOV[1]}_{IOV[2]}-{IOV[3]}.txt"

            IC = {}  # IC is a list of dictionaries, i.e. IC [ix] [iy]
            for ieta in range(-85, 86):
                IC[ieta] = {}

            mean_time = int(
                (results["IOV_info"]["time_end"] + results["IOV_info"]["time_begin"])
                / 2
            )
            xs.append(md.date2num(datetime.fromtimestamp(mean_time)))
            x_errs.append(
                [
                    xs[-1]
                    - md.date2num(
                        datetime.fromtimestamp(results["IOV_info"]["time_begin"])
                    ),
                    md.date2num(datetime.fromtimestamp(results["IOV_info"]["time_end"]))
                    - xs[-1],
                ]
            )
            h2_ic = ROOT.TH2F(
                "ic_map", "IC map; i#phi; i#eta", 360, 0.5, 360.5, 171, -85.5, 85.5
            )

            for harnessname in list(results["harness_map"].keys())[:]:
                E_ref = norm["harness_map"][harnessname][0]
                E_ref_err = norm["harness_map"][harnessname][1]

                Escale = results["harness_map"][harnessname][0]
                Eerr = results["harness_map"][harnessname][1]

                new_Escale = Escale / E_ref
                new_Eerr = np.sqrt(
                    Eerr**2 / E_ref**2 + Escale**2 * E_ref_err**2 / E_ref**4
                )

                if harnessname not in ys:
                    ys[harnessname] = [new_Escale]
                    y_errs[harnessname] = [new_Eerr]
                else:
                    ys[harnessname].append(new_Escale)
                    y_errs[harnessname].append(new_Eerr)

                if Escale <= 0.5 or Escale > 2.0:
                    ICvalue = E_ref
                    ICerr = E_ref_err
                else:
                    ICvalue = E_ref / Escale
                    ICerr = np.sqrt(
                        E_ref_err**2 / Escale**2 + E_ref**2 * Eerr**2 / Escale**4
                    )

                ICerr = 0.0  # the error of the IC was always set to 0

                ietamin, ietamax, iphimin, iphimax = EopCmlHandler.HarnessLimits(
                    harnessname
                )
                for ieta in range(ietamin, ietamax + 1):
                    for iphi in range(iphimin, iphimax + 1):
                        IC[ieta][iphi] = (ICvalue, ICerr)
                        ibin = h2_ic.FindBin(iphi, ieta)
                        h2_ic.SetBinContent(ibin, ICvalue)

                new_data = (new_Escale, new_Eerr)
                results["harness_map"][harnessname] = new_data

            c = ROOT.TCanvas(
                "c_%s" % results["IOV_info"]["runs_end"],
                "c_%s" % results["IOV_info"]["runs_end"],
                1400,
                700,
            )
            c.cd()
            ROOT.gStyle.SetOptStat(0)
            ROOT.gStyle.SetPalette(55)
            h2_ic.SetMinimum(0.80)
            h2_ic.SetMaximum(1.20)
            h2_ic.Draw("colz")
            c.Print(f"{plotsdir}/{run}/ic_map.png")
            c.Print(f"{plotsdir}/{run}/ic_map.root")
            del c
            del h2_ic

            basedir = eospath
            with open(f"{basedir}/{run}/results_norm.pkl", "wb") as file:
                pickle.dump(results, file)

            basedir = plotsdir
            os.makedirs(f"{basedir}/{run}/PointCorrections", exist_ok=True)
            EopCmlHandler.writeIC(f"{basedir}/{run}/PointCorrections/{IC_name}", IC)
            with open(f"{basedir}/{run}/results_norm.json", "w") as file:
                json.dump(results, file, indent=2)

        xs = np.array(xs)
        x_errs = np.array(x_errs).T
        ys = {k: np.array(ys[k]) for k in ys}
        y_errs = {k: np.array(y_errs[k]) for k in y_errs}

        cum_plotsdir = f"{plotsdir}/cumulative_plots"
        os.makedirs(cum_plotsdir, exist_ok=True)

        for harnessname in ys:
            result = {
                "x": xs,
                "xerr": x_errs,
                "y": ys[harnessname],
                "yerr": y_errs[harnessname],
            }
            result_path = f"{cum_plotsdir}/{harnessname}.pkl"
            with open(result_path, "wb") as file:
                pickle.dump(result, file)

        with open(f"{cum_plotsdir}/harness_map.json", "w") as file:
            json.dump(list(ys.keys()), file)
        print("Done normalize")

    def plot(harnessname, cum_plotsdir):
        # Load data
        result_path = f"{cum_plotsdir}/{harnessname}.pkl"
        with open(result_path, "rb") as file:
            result = pickle.load(file)
        x = result["x"]
        xerr = result["xerr"]
        y = result["y"]
        yerr = result["yerr"]

        fig, ax = plt.subplots(1, 1, figsize=(10, 7))
        ax.errorbar(x, y, xerr=xerr, yerr=yerr, fmt="ko")
        ax.margins(x=0.1, y=0.0)

        ymax = np.max(y + yerr)
        ymin = np.min(y - yerr)
        tot_range = ymax - ymin
        margin = 0.03
        ax.set_ylim(ymin - margin * tot_range, ymax + margin * tot_range)

        xmax = np.max(x + xerr[0, :])
        xmin = np.min(x - xerr[1, :])
        tot_range = xmax - xmin
        margin = 0.02
        ax.set_xlim(xmin - margin * tot_range, xmax + margin * tot_range)
        # ax.set_ylim(0.988, 1.00)

        ax.set_xlabel("Date", fontsize=14, loc="right")
        ax.set_ylabel("E/p scale (median)", fontsize=14, loc="top")
        ax.set_title(EopCmlHandler.format_harness(harnessname), fontsize=20)
        # ax.set_xticks(iovs)
        # ax.set_xticklabels(labels, rotation=90)

        start, end = ax.get_ylim()
        ax.yaxis.set_ticks(np.linspace(start, end, 20))

        start, end = ax.get_xlim()
        # ax.xaxis.set_ticks(np.linspace(start, end, int(len(iovs)/2)))
        ax.xaxis.set_ticks(np.linspace(start, end, 30))

        ax.set_xticklabels(ax.get_xticks(), rotation=45, ha="right")
        # xfmt = md.DateFormatter('%Y-%m-%d')
        xfmt = md.DateFormatter("%d-%m-%y")
        ax.xaxis.set_major_formatter(xfmt)

        ax.grid()

        fig.savefig(
            f"{cum_plotsdir}/{harnessname}.png",
            bbox_inches="tight",
        )
        plt.close()

    def process(self, files: List[str]) -> Tuple:
        runbasedir = "/".join(self.strip_path(files[0]).split("/")[:-1])
        print(runbasedir)
        run = runbasedir.split("/")[-1]
        eospath = "/".join(runbasedir.split("/")[:-1])

        # Merge all the results of a single 2/fb fill
        with open(f"{runbasedir}/splitting.json") as file:
            job_dictionary = json.load(file)

        with open(f"{runbasedir}/results.pkl", "rb") as file:
            info = pickle.load(file)

        for jobid in job_dictionary.keys():
            with open(f"{runbasedir}/results_{jobid}.pkl", "rb") as file:
                tmp = pickle.load(file)
                for key in tmp.keys():
                    info["harness_map"][key] = tmp[key]

        with open(f"{runbasedir}/results.pkl", "wb") as fo:
            pickle.dump(info, fo)
        with open(f"{runbasedir}/results.json", "w") as fo:
            json.dump(info, fo, indent=2)

        print("Creating point corrections")

        # create runbaseplotsdir
        runbaseplotsdir = f"{self.opts.plotsdir}/{run}"
        os.makedirs(runbaseplotsdir, exist_ok=True)

        # save json in plotsdir for web display
        with open(f"{runbaseplotsdir}/results.json", "w") as fo:
            json.dump(info, fo, indent=2)

        # create folder for point corrections (IOV, IC)
        icdir = f"{runbaseplotsdir}/PointCorrections/"
        os.makedirs(icdir, exist_ok=True)

        IOV_list = [
            [
                int(info["IOV_info"]["runs_begin"]),
                int(info["IOV_info"]["lumisection_begin"]),
                int(info["IOV_info"]["runs_end"]),
                int(info["IOV_info"]["lumisection_end"]),
            ]
        ]

        with open(f"{icdir}/IOVdictionary.txt", "w") as IOVdict_file:
            for iIOV, IOV in enumerate(IOV_list):
                icfilename = f"{icdir}/IC_{IOV[0]}-{IOV[1]}_{IOV[2]}-{IOV[3]}.txt"
                IOVdict_file.write(
                    "%i\t%i\t%i\t%i\t%s\n"
                    % (IOV[0], IOV[1], IOV[2], IOV[3], icfilename)
                )

        # Normalize Escales and ICs
        EopCmlHandler.normalize(eospath, self.opts.plotsdir)

        # Load harness map
        cum_plotsdir = f"{self.opts.plotsdir}/cumulative_plots"
        with open(f"{cum_plotsdir}/harness_map.json") as file:
            harnesses = json.load(file)

        print("Begin cumulative plots")
        with concurrent.futures.ProcessPoolExecutor(max_workers=10) as executor:
            tasks = []
            for harness in harnesses:
                tasks.append(executor.submit(EopCmlHandler.plot, harness, cum_plotsdir))
            concurrent.futures.wait(tasks)
            for task in tasks:
                task.result()
        # EopCmlHandler.plot(cum_plotsdir, harnesses)

        print("Done cumulative plots")

        return (
            [
                icfilename,
                runbasedir + "/results_norm.json",
                f"{self.opts.plotsurl}/{run}/",
            ],
            f"{self.opts.plotsurl}/cumulative_plots/",
        )

    def resubmit(self) -> int:
        """
        Resubmit failed runs.

        :return: status.
        """
        for (
            group
        ) in self.groups():  # grouping by int lumi, as specified in the decorator above
            # master run ---> to set the tag in the JobCtrl
            run = group[-1]
            fdict = self.get_files(group)  # get the files in the group
            if fdict is not None and len(fdict) > 0:
                jctrl = JobCtrl(
                    task=self.task,
                    campaign=self.campaign,
                    tags={"run_number": run["run_number"], "fill": run["fill"]},
                    dbname=self.opts.dbname,
                )
                if jctrl.taskExist() and len(jctrl.getFailed()) > 0:
                    files = jctrl.getJob(jid=0, last=True)[-1]["inputs"]
                    try:
                        jctrl.running(jid=0)

                        ret = self.process(
                            files=files,
                        )
                        if not ret:
                            jctrl.failed(jid=0)
                        else:
                            # mark as completed
                            jctrl.done(
                                jid=0,
                                fields={"output": ",".join(ret[0]), "plots": ret[1]},
                            )  # actually no plots are drawn now, to be implemented
                    except Exception as e:
                        print("Exception")
                        print("".join(tb.format_exception(None, e, e.__traceback__)))
                        jctrl.failed(jid=0)
                        self.log.error(
                            f'Failed producing the IC for run:  {run["run_number"]}: {e}'
                        )
                        continue

    def submit(self) -> int:
        """
        Submit calibration jobs for runs in eop-new
        """
        print("Submitting")
        for (
            group
        ) in self.groups():  # grouping by int lumi, as specified in the decorator above
            # master run ---> to set the tag in the JobCtrl
            run = group[-1]
            fdict = self.get_files(group)  # get the files in the group
            print(run, fdict)

            if fdict is not None and len(fdict) > 0:
                jctrl = JobCtrl(
                    task=self.task,
                    campaign=self.campaign,
                    tags={"run_number": run["run_number"], "fill": run["fill"]},
                    dbname=self.opts.dbname,
                )

                repr = run[self.task] == "reprocess"
                if not jctrl.taskExist() or repr:
                    print("Creating task")
                    jctrl.createTask(
                        jids=[0],
                        recreate=repr,
                        fields=[
                            {
                                "group": ",".join(
                                    [r["run_number"] for r in group[:-1]]
                                ),
                                "inputs": ",".join([f.split(",")[-1] for f in fdict]),
                            }
                        ],
                    )

                try:
                    print("Set jctrl to running")
                    jctrl.running(jid=0)
                    self.rctrl.updateStatus(
                        run=run["run_number"], status={self.task: "processing"}
                    )
                    for r in group[:-1]:
                        self.rctrl.updateStatus(
                            run=r["run_number"], status={self.task: "merged"}
                        )

                    ret = self.process(
                        files=[f.split(",")[-1] for f in fdict],
                    )
                    if not ret:
                        print("At the end the job failed")
                        jctrl.failed(jid=0)
                    else:
                        print("Job completed!")
                        # mark as completed
                        jctrl.done(
                            jid=0, fields={"output": ",".join(ret[0]), "plots": ret[1]}
                        )
                        print(",".join(ret[0]))
                        print(ret[1])
                except Exception as e:
                    # jctrl.failed(jid=0)
                    print("".join(tb.format_exception(None, e, e.__traceback__)))
                    self.rctrl.updateStatus(
                        run=run["run_number"], status={self.task: "reprocess"}
                    )
                    for r in group[:-1]:
                        self.rctrl.updateStatus(
                            run=r["run_number"], status={self.task: "reprocess"}
                        )

                    self.log.error(
                        f'Failed producing the IC for run:  {run["run_number"]}: {e}'
                    )
                    continue


if __name__ == "__main__":
    handler = EopCmlHandler(
        task="eop-cml",
        deps_tasks=["eop-mon"],
        prev_input="eop-mon",
    )
    ret = handler()
    sys.exit(ret)

get_opts = AutoCtrlScriptBase.export_options(EopCmlHandler)
