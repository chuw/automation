from typing import Dict, List, Callable
import re
import sys


def get_files_prev_task(self, runs: List[Dict]) -> List[str]:
    """
    Retrieve file names for given runs from a previous task.
    This function is added by :deco:`~ecalautoctrl.prev_task_data_source`.

    :param runs: list of input runs dictionaries (from :meth:`~ecalautoctrl.RunCtrl.getRuns`).
    """
    flist = []
    try:
        # run on output of previous step
        if self.prev_input:
            if isinstance(self.prev_input, str):
                self.prev_input = [self.prev_input]
            for prev_input in self.prev_input:
                flist.extend(
                    self.rctrl.getOutput(
                        runs=[run["run_number"] for run in runs], process=prev_input
                    )
                )
        return flist
    except AttributeError as ae:
        raise AttributeError(
            "Please define self.prev_input in your class when using the dbs_data_source decorator: "
            + str(ae)
        )


def prev_task_data_source(cls):
    """
    This decorator adds the a :func:`get_files` function provides input files from a previous
    task recorded in the automation influxdb database.
    It should be used to decorate a class that inherits from :class:`~ecalautoctrl.HandlerBase`.

    The decorator requires the data members :attr:`prev_input` to be defined in the class.
    Usually this has to be set in the derived class :meth:`__init__`.
    """
    setattr(cls, "get_files", get_files_prev_task)
    return cls


def process_by_intlumi(
    target: float = 0.0,
    nogaps: bool = True,
    group_filter: Callable[[Dict, Dict], bool] = lambda run1, run2: True,
):
    """
    This decorator adds the a :func:`groups` function that implements run splitting based on the amount of collected data.

    It should be used to decorate a class that inherits from :class:`~ecalautoctrl.HandlerBase`.

    :param target: target recorded integrated luminosity (in /pb).
    :param nogaps: do not allow gaps between runs in a given group. This implies that only subsequent runs for which the
    task is injected will be used to build a group. If a dependency is not fulfilled for a run the group is not processed.
    Therefore the integrated luminosity target is computed on all runs injected in the automation,
    regardless of the dependency status. This ensures building reproducible groups (default behaviour).
    If the option is False groups are built to reach the desired lumi figure regardless of continuity.
    :param group_filter: function to group together runs, might check for example that two runs are from the same year
    """

    def wrapper(cls):
        def groups(self) -> List[List[Dict]]:
            """
            Group runs to reach a desired integrated lumi.
            This function is added by :deco:`~ecalautoctrl.process_by_intlumi`.
            """
            # Groups are built from all runs injected for this task in case of no gaps. Dependencies in this case
            # are checked once the groups have been built.
            # Otherwise if gaps are allowed the dependencies are enforced at this stage.
            wdeps = {self.task: "new"}
            if not nogaps:
                wdeps.update(self.wdeps)
            runs = self.rctrl.getRuns(status=wdeps)
            wdeps = {self.task: "reprocess"}
            if not nogaps:
                wdeps.update(self.wdeps)
            runs.extend(self.rctrl.getRuns(status=wdeps))
            # sort by run number to create groups regardless if the tasks are in new or reprocess status
            runs = sorted(runs, key=lambda r: r["run_number"])

            if len(runs) == 0:
                print("Empty runs!")
                return [[]]

            grps: List[List[Dict]] = [[]]
            clumi = 0.0
            firstrun = runs[0]
            for run in runs:
                if group_filter(run, firstrun):
                    clumi += run["lumi"]
                    grps[-1].append(run)
                    # enough lumi, new group
                    if clumi >= target:
                        grps.append([])
                        clumi = 0.0
                else:
                    grps.append([])
                    clumi = 0.0
                    firstrun = run

                    clumi += run["lumi"]
                    grps[-1].append(run)
                    # enough lumi, new group
                    if clumi >= target:
                        grps.append([])
                        clumi = 0.0

            # remove the last group which is the growing one
            if clumi < target:
                grps.pop()

            grps = list(filter(lambda group: len(group) != 0, grps))
            # check dependencies in case nogaps is set.
            if nogaps:
                self.wdeps.update({self.task: "new"})
                available_runs = self.rctrl.getRuns(status=self.wdeps)
                self.wdeps.update({self.task: "reprocess"})
                available_runs.extend(self.rctrl.getRuns(status=self.wdeps))
                available_runs = [r["run_number"] for r in available_runs]

                # remove groups that do not fulfill the dependencies
                grps = [
                    grp
                    for grp in grps
                    if all(r["run_number"] in available_runs for r in grp)
                ]

            return grps

        setattr(cls, "groups", groups)
        return cls

    return wrapper


def same_year(run1: Dict, run2: Dict) -> bool:
    """
    Takes two runs and returns True only if they are from the same year
    """
    era1 = run1["era"]
    era2 = run2["era"]
    year1 = re.findall(r"\d+", era1)[0]
    year2 = re.findall(r"\d+", era2)[0]
    return year1 == year2
