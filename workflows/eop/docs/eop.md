# ECALELF ntuples production

## Resources
Quick links:

- [Jenkins job](https://dpg-ecal-calib.web.cern.ch/job/ecal-eop-prod/) 
- [Automation config](https://gitlab.cern.ch/cms-ecal-dpg/ECALELFS/automation/-/tree/master/workflows/eop)
- [Eop Framework](https://github.com/Fabio-mon/Eop_framework)

## Workflow structure
Takes ECALElf ntuple from both Z and W skim and uses `laserMonitoring.exe` from the Eop Framework to compute the E/p median scale for each harness.

The workflow runs on $2 fb^{-1}$ of data.

The automation workflow structure can be seen in the diagram below.

The `eop-mon` is the first Task to be run and it distributes over 12 jobs the measurements of the E/p scale for 27 harnesses.


What `eop-cml` does:
* gather the results merges them
* normalizes them to the first IOV of a certain year
* makes IC map plots
* IC corrections files (where IC = Escale_ref / Escale)


The results are presented as:
* `{run}/results_norm.json`: the E/p scales for each harness normalized to first IOV
* `{run}/PointsCorrections/IC_X_X_X_X.txt`: the IC map values
* `{run}/IC_map.(png/root)`: 2d map plot of ICs for each IOV
* `cumulative_plots/{harnessname}.png` where for each harness the E/p scale is presented as da function of the IOVs



![wflow-fig](eop-wflow.png)




