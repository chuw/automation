#!/usr/bin/env python3
import argparse
import subprocess
import sys
from typing import Dict
from typing import Optional, List, Tuple
from ecalautoctrl import JobCtrl, HTCHandler  # , process_by_intlumi
from ecalautoctrl.TaskHandlers import AutoCtrlScriptBase
from job_ctrl_helpers import (
    prev_task_data_source,
    process_by_intlumi,
    same_year,
)

# from ecalautoctrl.notifications import MattermostHandler
import harness_definition
import json
import pickle

import ROOT

ROOT.gROOT.SetBatch(1)
ROOT.gErrorIgnoreLevel = ROOT.kWarning  # switch off 'Info in <TCanvas::Print>: blabla'


@prev_task_data_source
@process_by_intlumi(target=2000, group_filter=same_year)
class EopHandler(HTCHandler):
    """
    Execute all the steps to generate the eop monitoring plots.
    Process fills that have been dumped (completed).

    :param task: task name.
    :param prev_input: name of the task from which gather the input data.
    :param deps_tasks: list of workfow dependencies.
    """

    def __init__(
        self,
        task: str,
        prev_input: str,
        deps_tasks: Optional[List[str]] = None,
        **kwargs,
    ):
        super().__init__(task=task, deps_tasks=deps_tasks, **kwargs)
        self.prev_input = prev_input
        self.submit_parser.add_argument(
            "--cfg",
            dest="cfg",
            default=None,
            type=str,
            help="Path to monitoring cfg file",
        )

        self.submit_parser.add_argument(
            "--outdir",
            dest="outdir",
            default=None,
            type=str,
            help="Base path of output location",
        )

        self.resubmit_parser.add_argument(
            "--cfg",
            dest="cfg",
            default=None,
            type=str,
            help="Path to monitoring cfg file",
        )

        self.resubmit_parser.add_argument(
            "--outdir",
            dest="outdir",
            default=None,
            type=str,
            help="Base path of output location",
        )

    def HarnessLimits(self, harnessname):
        if "/" in harnessname:
            for token in harnessname.split("/"):
                if "IEta" in token:
                    return self.HarnessLimits(token)

        return (
            int(harnessname.split("_")[1]),
            int(harnessname.split("_")[2]),
            int(harnessname.split("_")[4]),
            int(harnessname.split("_")[5]),
        )

    def icCreator(self, runs: List[int], files: List[str]) -> Tuple:  # noqa E501
        """
        Process eop data and produce the eop harness corrections.

        :return: ic output, plots.
        """
        runbasedir = f"{self.opts.outdir}/{runs[-1]}/"
        process = subprocess.Popen(
            f"mkdir -p {runbasedir}", shell=True, stdout=subprocess.PIPE
        )
        process.wait()
        # get the harness ranges
        print("Finally in IC creator")

        with open(self.opts.cfg) as fi:
            contents = fi.read()
            replaced_contents = contents.replace("INPUTFILES", " ".join(files))
            # replaced_contents = replaced_contents.replace("CMSSW_BASE", CMSSW_BASE)
            replaced_contents = replaced_contents.replace(
                "RUNS", f"{runs[0]} {int(runs[-1])+1}"
            )
            replaced_contents = replaced_contents.replace("OUTPUTFILE", "output.root")

        cfgfilename = runbasedir + "/config.cfg"

        with open(cfgfilename, "w") as fo:
            fo.write(replaced_contents)

        harness_ranges = harness_definition.GetHarnessRanges()
        # harness_ranges = harness_ranges[:3]

        harnessPerJob = 27
        job_dictionary = {}
        for i in range(int(len(harness_ranges) / harnessPerJob)):
            job_dictionary[i] = harness_ranges[
                i * harnessPerJob : (i + 1) * harnessPerJob
            ]

        with open(f"{runbasedir}/splitting.json", "w") as fo:
            json.dump(job_dictionary, fo, indent=2)

        # get start time, end time,
        #  ??? first lumi block of first run, last lumi block of last run
        df = ROOT.RDataFrame("selected", files)
        start = df.Min("eventTime")
        end = df.Max("eventTime")

        _runs = sorted(runs)

        runs_begin = _runs[0]
        runs_end = _runs[-1]
        time_begin = start.GetValue()
        time_end = end.GetValue()
        lumisection_begin = 0
        lumisection_end = 1e6
        del df

        info = {
            "IOV_info": {
                "runs_begin": runs_begin,
                "runs_end": runs_end,
                "time_begin": time_begin,
                "time_end": time_end,
                "lumisection_begin": lumisection_begin,
                "lumisection_end": lumisection_end,
            },
            "harness_map": {},
        }

        scaleValueBadHarness = -999.9
        for ih, harness_range in enumerate(harness_ranges):
            etamin = harness_range[0]
            etamax = harness_range[1]
            phimin = harness_range[2]
            phimax = harness_range[3]

            harness = f"IEta_{etamin}_{etamax}_IPhi_{phimin}_{phimax}"
            info["harness_map"][harness] = (scaleValueBadHarness, scaleValueBadHarness)

        with open(f"{runbasedir}/results.json", "w") as fo:
            json.dump(info, fo, indent=2)

        with open(f"{runbasedir}/results.pkl", "wb") as fo:
            pickle.dump(info, fo)

        return runbasedir, job_dictionary

    def resubmit(self):
        """
        Query for failed jobs and resubmit.
        Note: condor logs are overwritten.
        """

        runs = self.rctrl.getRuns(status={self.task: "processing"})

        for run_dict in runs:
            run = run_dict["run_number"]
            jctrl = JobCtrl(
                task=self.task,
                campaign=self.campaign,
                tags={"run_number": run, "fill": run_dict["fill"]},
                dbname=self.opts.dbname,
            )
            # check for evicted jobs. Jobs still marked as running in the db but not
            # acutally running in condor.
            for jid in jctrl.getRunning() + jctrl.getIdle():
                if not self.check_running_job(
                    jid=jctrl.getJob(jid=jid, last=True)[-1]["htc-id"]
                ):
                    jctrl.failed(jid=jid)

            # resubmit failed
            failed_jobs = jctrl.getFailed()
            if not jctrl.taskExist() or failed_jobs == []:
                self.log.info(f"no failures found for run {run}")
            else:
                self.log.info(f"found {len(failed_jobs)} failed job for run {run}")
                # Building the resubmission arguments
                # jobid, files
                # where jobid is the jobid of the original submission
                runbasedir = jctrl.getJob(0, last=True)[0]["runbasedir"]
                task = f'-w {self.task} -c {self.campaign} -t run_number:{run},fill:{run_dict["fill"]} --db {self.opts.dbname}'

                print("Failed jobs", failed_jobs)

                # eospath = "/eos/home-g/gpizzati/www/ecal_automation/eop/logs"
                eospath = "/eos/home-e/ecalgit/www/logs/"

                command = [
                    "condor_submit",
                    f"{self.opts.template}",
                    "-spool",
                    "-queue",
                    "HarnessId in " + f"{', '.join(failed_jobs)}",
                    "-append",
                    f"arguments = \"$(ClusterId).$(HarnessId) $(HarnessId) '{task}' {runbasedir} {self.opts.wdir}  \"",
                    "-append",
                    f"output = {eospath}/{self.task}-$(ClusterId)-$(HarnessId).log",
                    "-append",
                    f"error  = {eospath}/{self.task}-$(ClusterId)-$(HarnessId).log",
                ]

                print(command)

                ret = subprocess.run(
                    command,
                    capture_output=True,
                )

                # remove('args.txt')
                if ret.returncode == 0:
                    # awfully parse condor_submit output to get the clusterId
                    self.log.info(ret.stdout.decode().strip())
                    cluster = ret.stdout.decode().strip().split()[-1][:-1]
                    logurl = "https://ecallogs.web.cern.ch/"
                    # set the job status to idle and update the htc-id and log URL
                    for i, jid in enumerate(failed_jobs):
                        jctrl.idle(
                            jid=jid,
                            fields={
                                "htc-id": f"{cluster}.{i}",
                                "log": f"{logurl}/{self.task}-{cluster}-{jid}.log",
                            },
                        )
                else:
                    self.log.info(
                        "failed to submit jobs to condor. Error msg:"
                        + ret.stderr.decode()
                    )
                    return -1
        return 0

    def submit(self):
        """
        Submit runs in status task : new.
        Resubmit runs in status task : reprocess.
        """

        for group in self.groups():
            # master run
            run = group[-1]
            fdict = self.get_files(group)
            if fdict is not None and len(fdict) > 0:
                # fdict = [f'file:{f}' for f in fdict]
                jctrl = JobCtrl(
                    task=self.task,
                    campaign=self.campaign,
                    tags={"run_number": run["run_number"], "fill": run["fill"]},
                    dbname=self.opts.dbname,
                )
                # allow reprecessing only if previous jobs are not running
                allow_repr = run[self.task] == "reprocess"
                if jctrl.taskExist() and run[self.task] == "reprocess":
                    jobs = jctrl.getJobs()
                    for i in jobs["idle"] + jobs["running"]:
                        allow_repr &= not self.check_running_job(jid=i)
                if not jctrl.taskExist() or allow_repr:
                    task = f'-w {self.task} -c {self.campaign} -t run_number:{run["run_number"]},fill:{run["fill"]} --db {self.opts.dbname}'

                    # split processing such that each job runs on 1 harness

                    infiles = [f.split(",")[-1] for f in fdict]
                    runbasedir, job_dictionary = self.icCreator(
                        runs=[r["run_number"] for r in group], files=infiles
                    )
                    numJobs = len(list(job_dictionary.keys()))
                    # eospath = "/eos/home-g/gpizzati/www/ecal_automation/eop/logs"
                    eospath = "/eos/home-e/ecalgit/www/logs/"

                    command = [
                        "condor_submit",
                        f"{self.opts.template}",
                        "-spool",
                        "-queue",
                        "in "
                        + f"{', '.join(list(map(lambda k: str(k), range(numJobs))))}",
                        "-append",
                        f"arguments = \"$(ClusterId).$(ProcId) $(ProcId) '{task}' {runbasedir} {self.opts.wdir}  \"",
                        "-append",
                        f"output = {eospath}/{self.task}-$(ClusterId)-$(ProcId).log",
                        "-append",
                        f"error  = {eospath}/{self.task}-$(ClusterId)-$(ProcId).log",
                    ]
                    ret = subprocess.run(
                        command,
                        capture_output=True,
                    )
                    print(command)

                    # remove('args.txt')
                    if ret.returncode == 0:
                        # awfully parse condor_submit output to get the clusterId
                        self.log.info(
                            "Submitting jobs for run(s): "
                            + ",".join([r["run_number"] for r in group])
                            + f' fill {run["fill"]}'
                        )
                        self.log.info(ret.stdout.decode().strip())
                        cluster = ret.stdout.decode().strip().split()[-1][:-1]
                        logurl = "https://ecallogs.web.cern.ch/"
                        # logurl = "https://gpizzati.web.cern.ch/ecal_automation/eop/logs"
                        # create task injecting further job info:
                        # - input files
                        # - group: other runs processed by this task
                        # - log file
                        jctrl.createTask(
                            jids=list(range(numJobs)),
                            recreate=allow_repr,
                            fields=[
                                {
                                    "group": ",".join(
                                        [r["run_number"] for r in group[:-1]]
                                    ),
                                    "runbasedir": runbasedir,
                                    "inputs": ",".join(infiles),
                                    "part": str(i),
                                    "htc-id": f"{cluster}.{i}",
                                    "log": f"{logurl}/{self.task}-{cluster}-{i}.log",
                                }
                                for i in range(numJobs)
                            ],
                        )

                        # set the master run to status processing
                        self.rctrl.updateStatus(
                            run=run["run_number"], status={self.task: "processing"}
                        )
                        if len(group) > 1:
                            # set status merged for all other runs.
                            for r in group[:-1]:
                                self.rctrl.updateStatus(
                                    run=r["run_number"], status={self.task: "merged"}
                                )
                    else:
                        self.log.info(
                            "failed to submit jobs to condor. Error msg:"
                            + ret.stderr.decode()
                        )
                        return -1
        return 0


if __name__ == "__main__":
    handler = EopHandler(
        task="eop-mon",
        deps_tasks=["ecalelf-ntuples-wskim", "ecalelf-ntuples-zskim"],
        prev_input=["ecalelf-ntuples-wskim", "ecalelf-ntuples-zskim"],
    )
    ret = handler()
    sys.exit(ret)

get_opts = AutoCtrlScriptBase.export_options(EopHandler)
