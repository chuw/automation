#!/usr/bin/env python3
import sys
import os
import subprocess
from importlib import util
# import ROOT if possible (use mock import for docs generation)
try:
    if util.find_spec('ROOT'):
        import ROOT
except:
    from unittest.mock import MagicMock, patch
    my_conddb = MagicMock()
    patch.dict("sys.modules", fake_root=my_conddb).start()
    import fake_root as ROOT
from typing import Optional, Dict, List
from ecalautoctrl import JobCtrl, HandlerBase, prev_task_data_source, process_by_fill
from ecalautoctrl.CMSTools import QueryOMS
from ecalautoctrl.TaskHandlers import AutoCtrlScriptBase
from omsapi import OMSAPI
from condiovtools import *

@prev_task_data_source
@process_by_fill(fill_complete=True)
class Pi0MonCmlHandler(HandlerBase):
    """
    Update the cumulative pi0 mass history plot.

    :param task: task name.
    :param prev_input: name of the task from which gather the input data.
    :param deps_tasks: list of workfow dependencies.
    """

    def __init__(self,
                 task: str,
                 prev_input: str,
                 deps_tasks: Optional[List[str]]=None,
                 **kwargs):
        super().__init__(task=task, deps_tasks=deps_tasks, **kwargs)
        self.prev_input = prev_input

        self.submit_parser.add_argument('--eosplots',
                                        dest='eosplots',
                                        default=None,
                                        type=str,
                                        help='Plots webpage EOS path')
        self.submit_parser.add_argument('--plotsurl',
                                        dest='plotsurl',
                                        default=None,
                                        type=str,
                                        help='Plots webpage url')

        self.resubmit_parser.add_argument('--eosplots',
                                          dest='eosplots',
                                          default=None,
                                          type=str,
                                          help='Plots webpage EOS path')
        self.resubmit_parser.add_argument('--plotsurl',
                                          dest='plotsurl',
                                          default=None,
                                          type=str,
                                          help='Plots webpage url')
        
    def process_data(self, groups: List[Dict]):
        """
        Read the mass info files and produce the cumulative monitoring plots.
        """

        macro = './finalTimeVariationPlot.C'
        ROOT.gROOT.LoadMacro(macro)

        omsapi = OMSAPI("https://cmsoms.cern.ch/agg/api", "v1", cert_verify=False, verbose=False)
        omsapi.auth_oidc('ecalgit-omsapi', 'KXbuy4vxiETBc5C7FwteQxAF3X1irilx')
        omsquery = QueryOMS()

        for group in groups:
            # master run
            run = group[-1]
            fdict = self.get_files(group)
            if fdict is not None and len(fdict)>0:
                jctrl = JobCtrl(task=self.task,
                                campaign=self.campaign,
                                tags={'run_number' : run['run_number'], 'fill' : run['fill']},
                                dbname=self.opts.dbname)

                repr = (run[self.task] == 'reprocess')
                if not jctrl.taskExist() or repr:
                    jctrl.createTask(jids=[0],
                                     recreate=repr,
                                     fields=[{'group' : ','.join([r['run_number'] for r in group[:-1]])}])
                try:
                    jctrl.running(jid=0)
                    self.log.info(f'Processing fill {run["fill"]}.')
                    self.rctrl.updateStatus(run=run['run_number'], status={self.task : 'processing'})
                    for r in group[:-1]:
                        self.rctrl.updateStatus(run=r['run_number'], status={self.task : 'merged'})
                    eosdir = os.path.abspath(self.opts.eosplots)
                    plotsurl = self.opts.plotsurl
                    os.makedirs(eosdir, exist_ok=True)

                    # set the path to the cumulative file
                    cum_mass_info = os.path.abspath(eosdir+'/pi0_fitMassInfo_cumulative.txt')
                    # check if there is already a cumulative txt file and create an empty one if not
                    if not os.path.exists(cum_mass_info):
                        with open(cum_mass_info, 'w') as emptyfile:
                            pass  # empty block to create the file
                    points = {}
                    # add existing points first
                    self.log.info(f'Reading existing cumulative pi0 mass info file: ' + cum_mass_info)
                    with open(cum_mass_info, 'r') as cfile:
                        for lin in cfile:
                            l = lin.rstrip()
                            if len(l) > 0:
                                if l[0] == '#':
                                    continue
                                l_split = l.split(' ')
                                if len(l_split) > 0 and len(l_split[0]) > 0:
                                    points[float(l_split[0])] = l
                    for newf in fdict:
                        self.log.info(f'Reading new pi0 mass info file: ' + newf)
                        with open(newf, 'r') as rfile:
                            for i, lin in enumerate(rfile):
                                l = lin.rstrip()
                                if i > 0 and len(l) > 0:
                                    if l[0] == '#':
                                        continue
                                    l_split = l.split(' ')
                                    if len(l_split) > 0 and len(l_split[0]) > 0:
                                        # use a dictionary indexed by the point timestamp to avoid double counting.
                                        points[float(l_split[0])] = l
                    self.log.info(f'Writing updated pi0 cumulative mass info file: ' + cum_mass_info)
                    with open(cum_mass_info, 'w') as cfile:
                        for t, l in points.items():
                            cfile.write(l + '\n')
                                        
                    self.log.info(f'Writing file-list file.')
                    with open("timeVariationFileList.txt", "w") as finalin:
                        finalin.write('#TextFile,Label,Color\n')
                        finalin.write(f'{cum_mass_info},With Light Monitoring Correction,417')

                    self.log.info(f'Getting runs for conditions IOVs from CondDB.')
                    timingRcdLabel = 'CC'
                    gt = run["globaltag"]
                    # tags for which IOV updates can be shown in the plot
                    psiovruns = get_cond_iov_runs(get_tag(gt, "EcalPulseShapesRcd"), "EcalCond")
                    iciovruns = get_cond_iov_runs(get_tag(gt, "EcalIntercalibConstantsRcd"), "EcalCond")
                    #timeiovruns = get_cond_iov_runs(get_tag(gt, "EcalTimeCalibConstantsRcd", timingRcdLabel), "EcalCond")
                    ebaligniovruns = get_cond_iov_runs(get_tag(gt, "EBAlignmentRcd"), "Alignments")
                    eealigniovruns = get_cond_iov_runs(get_tag(gt, "EEAlignmentRcd"), "Alignments")
                    esaligniovruns = get_cond_iov_runs(get_tag(gt, "ESAlignmentRcd"), "Alignments")
                    #pediovruns = get_cond_iov_runs(get_tag(gt, "EcalPedestalsRcd"), "EcalCond")

                    # tags for which IOV updates are shown in the plot
                    tagiovs = {"PS":psiovruns,
                               #"T_{" + timingRcdLabel + "}":timeiovruns,
                               "IC":iciovruns,
                               "A^{EB}":ebaligniovruns,
                               "A^{EE}":eealigniovruns,
                               "A^{ES}":esaligniovruns,
                               #"P":pediovruns
                              }

                    runcondstrs = merge_iovruns(tagiovs)
                    iovruns = [r for r in sorted(runcondstrs) if r > 347000]
                    runstrs, run_start_times = get_run_info(iovruns, omsquery, omsapi)
                    condstrs = ["{0}-{1}".format(runcondstrs[int(r)], r) for r in runstrs]

                    self.log.info(f'Generating pi0 mass time variation plots.')
                    ROOT.finalTimeVariationPlot("timeVariationFileList.txt", eosdir+'/', True, condstrs, run_start_times, [2022, 2023, 2024, 2025])

                    # mark as completed
                    jctrl.done(jid=0, fields={
                        'output' : cum_mass_info,
                        'plots' : plotsurl})

                except Exception as e:
                    jctrl.failed(jid=0)
                    self.log.error(f'Failed producing the monitoring plots for fill {run["fill"]}: {e}')
                    continue
    
    def submit(self):
        """
        Produce cumulative pi0 mass monitoring plots
        """

        self.process_data(groups=self.groups())

        return 0

    def resubmit(self):
        """
        Resubmit failed jobs
        """

        # get runs in status processing
        runs = self.rctrl.getRuns(status = {self.task : 'processing'})

        # check if any job failed before resubmitting
        for run_dict in runs:
            jctrl = JobCtrl(task=self.task,
                            campaign=self.campaign,
                            tags={'run_number' : run_dict['run_number'],
                                  'fill' : run_dict['fill']},
                            dbname=self.opts.dbname)
            if jctrl.taskExist() and len(jctrl.getFailed()) > 0:
                self.process_data(groups=[[run_dict]])

        return 0

if __name__ == '__main__':
    handler = Pi0MonCmlHandler(task='pi0-mon-cml',
                               deps_tasks=['pi0-mon'],
                               prev_input='pi0-mon')

    ret = handler()

    sys.exit(ret)

get_opts = AutoCtrlScriptBase.export_options(Pi0MonCmlHandler)
