#!/usr/bin/env python3
# collect all the files from the fill and create a configuration file
import sys
from TICMeanTimeHandler import TICMeanTimeHandler

if __name__ == '__main__':
    handler = TICMeanTimeHandler(task='timing-cc-val',
                                 deps_tasks=['timing-cc-reco'],
                                 prev_input='timing-cc-reco',
                                 is_cc=True,
                                 is_rereco=False)

    ret = handler()

    sys.exit(ret)
