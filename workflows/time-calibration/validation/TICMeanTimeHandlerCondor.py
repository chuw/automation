#!/usr/bin/env python3
import os
import subprocess
import operator, itertools
from typing import Optional, Dict, List
from ecalautoctrl import HTCHandler, JobCtrl, process_by_fill, prev_task_data_source

@prev_task_data_source
@process_by_fill(fill_complete=True)
class TICMeanTimeHandlerCondor(HTCHandler):
    """
    :param task: task name.
    :param prev_input: name of the task from which gather the input data.
    :param deps_tasks: list of workfow dependencies.
    """
    def __init__(self,
                 task: str,
                 prev_input: str,
                 deps_tasks: Optional[List[str]]=None,
                 is_cc: bool=False,
                 is_rereco: bool=False,
                 **kwargs):

        if not deps_tasks:
            deps_tasks = [prev_input]
        else:
            deps_tasks.append(prev_input)

        super().__init__(task=task, deps_tasks=deps_tasks, **kwargs)

        self.prev_input = prev_input
        self.is_cc = is_cc
        self.is_rereco = is_rereco
        self.tag = 'EcalTimeCalibConstants'

        # custom options
        self.submit_parser.add_argument('--min-energy-eb',
                                        dest='min_energy_eb',
                                        default=0.5,
                                        type=float,
                                        help='Minimum RecHit energy EB (GeV)')
        self.submit_parser.add_argument('--min-energy-ee',
                                        dest='min_energy_ee',
                                        default=0.5,
                                        type=float,
                                        help='Minimum RecHit energy EE (GeV)')
        self.submit_parser.add_argument('--eosplots',
                                        dest='eosplots',
                                        default=None,
                                        type=str,
                                        help='Plots webpage EOS path')
        self.submit_parser.add_argument('--plotsurl',
                                        dest='plotsurl',
                                        default=None,
                                        type=str,
                                        help='Plots webpage url')

        self.resubmit_parser.add_argument('--min-energy-eb',
                                        dest='min_energy_eb',
                                        default=0.5,
                                        type=float,
                                        help='Minimum RecHit energy EB (GeV)')
        self.resubmit_parser.add_argument('--min-energy-ee',
                                        dest='min_energy_ee',
                                        default=0.5,
                                        type=float,
                                        help='Minimum RecHit energy EE (GeV)')
        self.resubmit_parser.add_argument('--eosplots',
                                          dest='eosplots',
                                          default=None,
                                          type=str,
                                          help='Plots webpage EOS path')        
        self.resubmit_parser.add_argument('--plotsurl',
                                          dest='plotsurl',
                                          default=None,
                                          type=str,
                                          help='Plots webpage url')

    def group_files(self, files: List[str]):
        """
        Group all the files from the previous step and write to a file.

        :param files: list of files to merge.       
        """
        filelist = ','.join(files)
        return filelist

    def submit_jobs(self, groups: List[Dict], resubmit: bool=False):
        """
        Submit condor job

        For reco: save payloads / run RunTimeAverage, produce dat and plots / save sqlite file
        For rereco: run RunTimeAverage, produce dat and plots

        :param groups: groups of runs to be processed.
        :param resubmit: resubmission of failed jobs.
        """
        for group in groups:
            # master run
            mrun = group[-1]

            fdict = self.get_files(group)
            if fdict is not None and len(fdict)>0:
                mrun_number = mrun["run_number"]
                fill = mrun["fill"]
                jctrl = JobCtrl(task=self.task,
                                campaign=self.campaign,
                                tags={'run_number':mrun_number, 'fill':fill},
                                dbname=self.opts.dbname)

                # check for evicted jobs. Jobs still marked as running in the db but not
                # actually running in condor.
                for jid in jctrl.getRunning():
                    if not self.check_running_job(jid = jctrl.getJob(jid=jid, last=True)[-1]['htc-id']):
                        self.log.info(f"Found job {jid} of fill {fill} marked as running but not listed on condor. Setting job status to failed.")
                        jctrl.failed(jid=jid)

                # allow reprocessing only if previous jobs are not running
                allow_repr = (mrun[self.task] == 'reprocess')
                if jctrl.taskExist() and mrun[self.task] == 'reprocess':
                    jobs = jctrl.getJobs()
                    if len(jobs['idle']+jobs['running']) > 0:
                        self.log.info(f"Reprocessing of run {mrun_number} not allowed because {len(jobs['idle']+jobs['running'])} job(s) are in 'idle' or 'running' state.")
                        allow_repr = False

                failedjobs = jctrl.getFailed()
                if not jctrl.taskExist() or len(failedjobs) > 0 or allow_repr:
                    condor_resubmit_args = []
                    if len(failedjobs) > 0 and not allow_repr:
                        self.log.info(f'Found {len(failedjobs)} failed job(s) for fill {fill}')
                        if resubmit:
                            condor_resubmit_args = ['-append', f'+JobFlavour = "{self.opts.resubflv}"']

                    task = f'-w {self.task} -c {self.campaign} -t run_number:{mrun_number},fill:{fill} --db {self.opts.dbname}'
                    files = self.group_files(fdict)

                    runs = []
                    for r in group:
                        runs.append(r["run_number"])
                    runs = ','.join(runs)

                    condor_submit_cmd = ['condor_submit',
                                        f'{self.opts.template}',
                                         '-spool',
                                         '-queue', '1',
                                         '-append', f'arguments = "$(ClusterId).$(ProcId) 0 {files} \'{task}\' {self.opts.eosdir} {self.opts.wdir} {self.opts.eosplots} {self.opts.plotsurl} {runs} {fill} {self.is_rereco} {self.is_cc} {self.tag} {mrun["globaltag"]} {self.opts.min_energy_eb} {self.opts.min_energy_ee}"']

                    ret = subprocess.run(condor_submit_cmd + condor_resubmit_args, capture_output=True)
                
                    if ret.returncode == 0:
                        # awfully parse condor_submit output to get the clusterId
                        self.log.info(f'Submitting jobs for run(s): '+','.join([r['run_number'] for r in group])+f' fill {fill}')
                        self.log.info(ret.stdout.decode().strip())
                        cluster = ret.stdout.decode().strip().split()[-1][:-1]
                        logurl = 'https://ecallogs.web.cern.ch/'
                        if not resubmit:
                            # create task injecting further job info:
                            # - group: other runs processed by this task
                            # - htc-id: HTCondor job ID
                            # - log file
                            jctrl.createTask(jids=[0],
                                             recreate=allow_repr,
                                             fields=[{'group': ','.join([r['run_number'] for r in group[:-1]]),
                                                      'htc-id': f"{cluster}.0",
                                                      'log': f'{logurl}/{self.task}-{cluster}-0.log'}])
                            # set the master run to status processing
                            self.rctrl.updateStatus(run=mrun_number, status={self.task: 'processing'})
                            if len(group) > 1:
                                # set status merged for all other runs.
                                for r in group[:-1]:
                                    self.rctrl.updateStatus(run=r['run_number'], status={self.task: 'merged'})
                        else:
                            # set the job status to idle and update the htc-id and log file path
                            for jid in failedjobs:
                                jctrl.idle(jid=jid, fields={'htc-id': f"{cluster}.0", 'log': f'{logurl}/{self.task}-{cluster}-0.log'})
                    else:
                        self.log.info("failed to submit jobs to condor. Error msg:"+ret.stderr.decode())
                        return -1

        return 0

    def submit(self):
        """
        Execute the local merging step. 
        Reprocess re-injected runs as well.
        """

        self.submit_jobs(groups=self.groups())
  
        return 0
            
    def resubmit(self):
        """
        Resubmit runs with failed jobs
        """
        # get runs in status processing and merged
        runs = self.rctrl.getRuns(status = {self.task : 'merged'})
        runs += self.rctrl.getRuns(status = {self.task : 'processing'})

        # group the runs by fill
        fill_groups = []
        runs = sorted(runs, key=operator.itemgetter('fill'))
        for i,g in itertools.groupby(runs, key=operator.itemgetter('fill')):
            fill_groups.append(list(g))
 
        # re-process the data by fill group
        for group in fill_groups:
            self.submit_jobs(groups=[group], resubmit=True)
        return 0


