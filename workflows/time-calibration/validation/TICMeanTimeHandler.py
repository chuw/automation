#!/usr/bin/env python3
import os
import subprocess
import operator, itertools
from typing import Optional, Dict, List
from ecalautoctrl import HandlerBase, JobCtrl, process_by_fill, prev_task_data_source

@prev_task_data_source
@process_by_fill(fill_complete=True)
class TICMeanTimeHandler(HandlerBase):
    """
    Run all steps of the pulse shapes measurement.

    :param task: task name.
    :param prev_input: name of the task from which gather the input data.
    :param deps_tasks: list of workfow dependencies.
    """
    def __init__(self,
                 task: str,
                 prev_input: str,
                 deps_tasks: Optional[List[str]]=None,
                 is_cc: bool=False,
                 is_rereco: bool=False,
                 **kwargs):

        if not deps_tasks:
            deps_tasks = [prev_input]
        else:
            deps_tasks.append(prev_input)

        super().__init__(task=task, deps_tasks=deps_tasks, **kwargs)

        self.prev_input = prev_input
        self.is_cc = is_cc
        self.is_rereco = is_rereco
        self.tag = 'EcalTimeCalibConstants'

        # custom options
        self.submit_parser.add_argument('--min-energy-eb',
                                        dest='min_energy_eb',
                                        default=0.5,
                                        type=float,
                                        help='Minimum RecHit energy EB (GeV)')
        self.submit_parser.add_argument('--min-energy-ee',
                                        dest='min_energy_ee',
                                        default=0.5,
                                        type=float,
                                        help='Minimum RecHit energy EE (GeV)')
        self.submit_parser.add_argument('--eosplots',
                                        dest='eosplots',
                                        default=None,
                                        type=str,
                                        help='Plots webpage EOS path')
        self.submit_parser.add_argument('--plotsurl',
                                        dest='plotsurl',
                                        default=None,
                                        type=str,
                                        help='Plots webpage url')

        self.resubmit_parser.add_argument('--min-energy-eb',
                                        dest='min_energy_eb',
                                        default=0.5,
                                        type=float,
                                        help='Minimum RecHit energy EB (GeV)')
        self.resubmit_parser.add_argument('--min-energy-ee',
                                        dest='min_energy_ee',
                                        default=0.5,
                                        type=float,
                                        help='Minimum RecHit energy EE (GeV)')
        self.resubmit_parser.add_argument('--eosplots',
                                          dest='eosplots',
                                          default=None,
                                          type=str,
                                          help='Plots webpage EOS path')
        self.resubmit_parser.add_argument('--plotsurl',
                                          dest='plotsurl',
                                          default=None,
                                          type=str,
                                          help='Plots webpage url')

    def group_files(self, files: List[str]):
        """
        Group all the files from the previous step and write to a file.

        :param files: list of files to merge.
        """
        filelist = ','.join(files)
        return filelist

    def get_mean_time_cmd(self, files, fill, outdir, nSigma=2, maxRange=10, ebECut=0.5, eeECut=0.5):
        """
        Run the EcalTimingCalibration_cfg using the input files from the previous
        step. The input arguments can be customized according to the needs.

        ** The values need to be read by a json file in the future **

        :nSigma: Number of sigma (x2) window around the mean for hit selection in each xtal
        :maxRange: Maximum range of hit time (ns). Default is +/- 10 ns.
        :ebECut: Cuts on the hit energies in the barrel.
        :eeECut: Cuts on the hit energies in the endcaps.
        """

        cmd = f'RunTimeAverage --files={self.group_files(files)} --nSigma={nSigma} --maxRange={maxRange} --ebECut={ebECut} --eeECut={eeECut} --outputCalibCorr=ecalTiming-corr_{fill}.dat --outputFile=ecalTiming_{fill}.root --outputDir={outdir}/'

        print(cmd)
        return cmd.split()

    def process_data(self, groups: List[Dict]):
        """
        Execute the merging and plotting step for a single run.
        The merging step compute average pulse shapes weighting each
        input file by the number of hits (separately for each channel).

        :param groups: groups of run to be processed.
        """

        for i, group in enumerate(groups):
            # only process up to 5 groups in one Jenkins build to allow downstream tasks in other runs to be processed as well
            if i > 4:
                break

            self.log.info('Processing run(s): '+','.join([r['run_number'] for r in group]))

            # reset output
            outputs = {}

            # gather master run details
            mrun = group[-1]
            run = mrun['run_number']
            fill = mrun['fill']
            gt = mrun['globaltag']
            jctrl = JobCtrl(task=self.task,
                            campaign=self.campaign,
                            tags={'run_number' : run, 'fill' : mrun['fill']},
                            dbname=self.opts.dbname)

            repr = (mrun[self.task] == 'reprocess')
            if not jctrl.taskExist() or repr:
                jctrl.createTask(jids=[0],
                                 recreate=repr,
                                 fields=[{'group' : ','.join([r['run_number'] for r in group[:-1]])}])
            try:
                # set the status of the runs as processing
                self.rctrl.updateStatus(run=run, status={self.task : 'processing'})
                if len(group) > 1:
                    # set status merged for all other runs.
                    for r in group[:-1]:
                        self.rctrl.updateStatus(run=r['run_number'], status={self.task : 'merged'})
                jctrl.running(jid=0)

                # output directory
                if self.is_rereco:
                    outdir = self.opts.eosdir + f'/validation-rereco/{fill}/'
                else:
                    outdir = self.opts.eosdir + f'/validation-reco/{fill}/'

                if not os.path.isdir(outdir):
                    os.makedirs(outdir)

                # save payload
                if not self.is_rereco:
                    self.log.info('Run subprocess for saving reference payload')
                    ret = subprocess.run(['bash', 'validation/savePayload.sh', run, gt, str(self.is_cc), outdir], capture_output=False)
                    if ret.returncode != 0:
                        jctrl.failed(jid=0)
                        self.log.error(f'Failed saving reference payload for fill {fill}')
                        continue

                # get files
                files = self.get_files(group)
                dat_file = os.path.abspath(outdir+(f'/ecalTiming-corr_{fill}.dat'))

                # produce dat file
                self.log.info('Run subprocess for producing dat files')
                ret = subprocess.run(self.get_mean_time_cmd(files, fill, outdir, 2, 10, self.opts.min_energy_eb, self.opts.min_energy_ee),
                                     capture_output=False)

                # plot from the dat file, saved to eosdir
                eosplots = self.opts.eosplots + f'/{fill}/'
                if not os.path.isdir(eosplots):
                    os.makedirs(eosplots)

                # plots to be shown at grafana monitoring page
                plotsurl = self.opts.plotsurl + f'/{fill}/'

                ret = subprocess.run(['python3', 'validation/plot_calibration.py', f'{outdir}/ecalTiming-corr_{fill}.dat', str(self.is_rereco), eosplots], capture_output=False)

                # save sqlite
                if not self.is_rereco:
                    self.log.info('Run subprocess for producing sqlite')
                    # save the sqlite file in the outdir (/SOME_BASE_DIR/$FILL/)
                    sqlite_file = os.path.abspath(outdir+(f'/ecalTiming-abs_{fill}.db'))
                    ret = subprocess.run(['bash', 'validation/produceSqlite.sh', fill, self.tag, outdir], capture_output=False)

                    # symlink it to dest_dir (/SOME_BASE_DIR/rereco/$RUN/) for future reference (rereco step will fetch sqlite from the dest_dir)
                    for r in group:
                        dest_dir = os.path.abspath(self.opts.eosdir+f'/rereco/{r["run_number"]}/')

                        if not os.path.isdir(dest_dir):
                            os.makedirs(dest_dir)

                        os.system(f'ln -sf {sqlite_file} {dest_dir}/ecalTiming-abs.db')

                    outputs.update({'output' : dat_file, 'sqlite': sqlite_file, 'plots': plotsurl })
                else:
                    outputs.update({'output' : dat_file, 'plots': plotsurl })

                # remove the stuff that will no longer be used
                os.system(f'rm {outdir}/ecalTiming.dat')
                os.system(f'rm {outdir}/ecalTiming_{fill}.root')

                jctrl.done(jid=0, fields=outputs)

            except Exception as e:
                jctrl.failed(jid=0)
                self.log.error(f'Failed processing merging step for fill {fill}: {e}')

    def submit(self):
        """
        Execute the local merging step.
        Reprocess re-injected runs as well.
        """

        self.process_data(groups=self.groups())

        return 0

    def resubmit(self):
        """
        Resubmit runs with failed jobs
        """
        # get runs in status failed
        runs = self.rctrl.getRuns(status = {self.task : 'failed'})

        # group the runs by fill
        fill_groups = []
        runs = sorted(runs, key=operator.itemgetter('fill'))
        for i,g in itertools.groupby(runs, key=operator.itemgetter('fill')):
            fill_groups.append(list(g))

        # re-process the data by fill group
        for group in fill_groups:
            self.process_data(groups=[group])

        return 0

