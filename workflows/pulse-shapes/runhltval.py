#!/usr/bin/env python3
import sys
import shutil
import subprocess
import logging
from os import makedirs, path, system
from itertools import islice
from typing import List, Optional
from ecalautoctrl import JobCtrl, HTCHandler, process_by_fill, dbs_data_source
from ecalautoctrl.TaskHandlers import AutoCtrlScriptBase

@dbs_data_source
@process_by_fill(fill_complete=True)
class PSValidationHandler(HTCHandler):
    """
    Run the HLT rate validation, either with reference prompt conditions or
    with the pulse shape payload under test.
    
    :param task: task name. The "ref" or "val" suffix will be automatically
    appended based on the value of the `--ref` command line option.
    :param deps_tasks: list of workfow dependencies.
    :param dsetname: input RAW dataset name.
    """

    def __init__(self,
                 task: str,
                 deps_tasks: List[str]=None,
                 dsetname: str=None,
                 **kwargs):        
        super().__init__(task=task, deps_tasks=deps_tasks, **kwargs)

        self.dsetname = dsetname
        
        self.parser.add_argument('--ref',
                                 dest='refcond',
                                 default=False,
                                 action='store_true',
                                 help='Run with reference conditions if set, validation one otherwise')

        
    def __call__(self):
        """
        Execute the command specified by the command line options.
        A suffix is appended to the task name based on the `--ref` option.
        """

        # parse the cmd line to adjust the task name
        self.opts = self.parser.parse_args()
        self.task += 'ref' if self.opts.refcond else 'val'

        # Update appname for log messages but do not update self.appname itself to avoid issues with DB queries
        self.stream_handler.setFormatter(logging.Formatter(f'[{self.task.upper()}]: %(message)s'))

        # standard call
        return super().__call__()

    def submit(self):
        """
        Get the HLT menu and customise it before submitting the jobs.
        Then submit runs in status task: new
.       Or resubmit runs in status task: reprocess.
        """

        for group in self.groups():
            fdict = self.get_files(group)
            if fdict is not None and len(fdict)>0:
                # master run
                run = group[-1]
                fill = run['fill']
                task = f'-w {self.task} -c {self.campaign} -t run_number:{run["run_number"]},fill:{fill} --db {self.opts.dbname}'
                # get the HLT menu to be submitted for this group
                subprocess.run(['bash', 'getHltMenu.sh', fdict[0], run['globaltag'], task], capture_output=False)

                # save HLT menu for resubmissions
                outdir = path.abspath(self.opts.eosdir + f'/{fill}/')
                if not path.isdir(outdir):
                    makedirs(outdir)
                menufile = 'hltref.py' if self.opts.refcond else 'hltval.py'
                system(f'cp hlt.py {outdir}/{menufile}')

                jctrl = JobCtrl(task=self.task,
                                campaign=self.campaign,
                                tags={'run_number': run['run_number'], 'fill': run['fill']},
                                dbname=self.opts.dbname)
                # allow reprecessing only if previous jobs are not running
                allow_repr = (run[self.task] == 'reprocess')
                if jctrl.taskExist() and run[self.task] == 'reprocess':
                    jobs = jctrl.getJobs()
                    if len(jobs['idle']+jobs['running']) > 0:
                        self.log.info(f"Reprocessing of run {run['run_number']} not allowed because {len(jobs['idle']+jobs['running'])} job(s) are in 'idle' or 'running' state.")
                        allow_repr = False
                if not jctrl.taskExist() or allow_repr:
                    # split the file list into groups accordingly to nfiles,
                    # apexes are added around join(f) to ensure condor will later
                    # interpret the comma separated list as a single item
                    eosdir = path.abspath(self.opts.eosdir+f'/{run["run_number"]}/')
                    self.opts.nfiles = self.opts.nfiles if self.opts.nfiles else len(fdict)
                    it = iter(fdict)
                    flist = [",".join([f for f in ff]) for ff in iter(lambda: tuple(islice(it, self.opts.nfiles)), ())]
                    with open('args.txt', 'w') as ff:
                        ff.write('\n'.join([f for f in flist]))
                    # get the global tag from the influxdb.
                    # if not set leave GT empty (rely on auto cond).
                    gt = run['globaltag'] if 'globaltag' in run else '\'\''
                    self.log.info(f'GlobalTag: {gt}')
                    ret = subprocess.run(['condor_submit',
                                          f'{self.opts.template}',
                                          '-spool',
                                          '-queue', '1 fname from args.txt',
                                          '-append', f'arguments = "$(ClusterId).$(ProcId) $(ProcId) $(fname) \'{task}\' {eosdir} {self.opts.wdir} {gt}"'],
                                         capture_output=True)
                    #remove('args.txt')
                    if ret.returncode == 0:
                        # awfully parse condor_submit output to get the clusterId
                        self.log.info(f'Submitting jobs for run(s): '+','.join([r['run_number'] for r in group])+f' fill {run["fill"]}')
                        self.log.info(ret.stdout.decode().strip())
                        cluster = ret.stdout.decode().strip().split()[-1][:-1]
                        logurl = 'https://ecallogs.web.cern.ch/'
                        # create task injecting further job info:
                        # - input files
                        # - group: other runs processed by this task
                        # - htc-id: HTCondor job ID
                        # - log file
                        jctrl.createTask(jids=list(range(len(flist))),
                                         recreate=allow_repr,
                                         fields=[{'group': ','.join([r['run_number'] for r in group[:-1]]),
                                                  'inputs': f,
                                                  'htc-id': f"{cluster}.{i}",
                                                  'log': f'{logurl}/{self.task}-{cluster}-{i}.log'} for i,f in enumerate(flist)])
                        # set the master run to status processing
                        self.rctrl.updateStatus(run=run['run_number'], status={self.task: 'processing'})
                        if len(group) > 1:
                            # set status merged for all other runs.
                            for r in group[:-1]:
                                self.rctrl.updateStatus(run=r['run_number'], status={self.task: 'merged'})
                    else:
                        self.log.info("failed to submit jobs to condor. Error msg:"+ret.stderr.decode())
                        return -1
        return 0

    def resubmit(self):
        """
        Query for failed jobs and resubmit with HLT menu saved during submit.
        """

        runs = self.rctrl.getRuns(status={self.task: 'processing'})

        for run_dict in runs:
            run = run_dict['run_number']
            fill = run_dict['fill']
            jctrl = JobCtrl(task=self.task,
                            campaign=self.campaign,
                            tags={'run_number': run, 'fill': fill},
                            dbname=self.opts.dbname)
            # check for evicted jobs. Jobs still marked as running in the db but not
            # actually running in condor.
            for jid in jctrl.getRunning():
                if not self.check_running_job(jid = jctrl.getJob(jid=jid, last=True)[-1]['htc-id']):
                    jctrl.failed(jid=jid)

            # resubmit failed
            jobs = jctrl.getJobs()
            failedjobs = jobs['failed']
            n_idlejobs = len(jobs['idle'])
            n_runningjobs = len(jobs['running'])
            n_donejobs = len(jobs['done'])
            n_jobs = len(failedjobs) + n_idlejobs + n_runningjobs + n_donejobs
            if not jctrl.taskExist() or failedjobs == []:
                self.log.info(f'No failures found for run {run} with {n_jobs} job(s). Job status: {n_idlejobs} idle, {n_runningjobs} running, {n_donejobs} done, {len(failedjobs)} failed.')
            else:
                self.log.info(f'Found {len(failedjobs)} failed job(s) for run {run} with {n_jobs} job(s). Job status: {n_idlejobs} idle, {n_runningjobs} running, {n_donejobs} done, {len(failedjobs)} failed.')
                # Building the resubmission argumets
                # jobid, files
                # where jobid is the jobid of the original submission
                eosdir = path.abspath(self.opts.eosdir+f'/{run}/')
                # move to AAA if file can't be read from T0 eos.
                flist = []
                for i in failedjobs:
                    inputs = jctrl.getJob(i, last=True)[0]['inputs']
                    if '/eos/cms/tier0' in inputs and not all([path.isfile(p) for p in inputs.split(',')]):
                        inputs = inputs.replace('file:/eos/cms/tier0', 'root://cms-xrd-global.cern.ch/')
                    flist.append(i+', '+inputs)
                task = f'-w {self.task} -c {self.campaign} -t run_number:{run},fill:{fill} --db {self.opts.dbname}'

                # retrieve HLT menu
                menudir = path.abspath(self.opts.eosdir + f'/{fill}/')
                menufile = 'hltref.py' if self.opts.refcond else 'hltval.py'
                system(f'cp {menudir}/{menufile} hlt.py')

                with open('args.txt', 'w') as ff:
                    ff.write('\n'.join(flist))
                # get the global tag from the influxdb.
                # if not set leave GT empty (rely on auto cond).
                gt = run_dict['globaltag'] if 'globaltag' in run_dict else '\'\''
                self.log.info(f'GlobalTag: {gt}')
                ret = subprocess.run(['condor_submit',
                                      f'{self.opts.template}',
                                      '-spool',
                                      '-queue', 'jobid, fname from args.txt',
                                      '-append', f'arguments = "$(ClusterId).$(ProcId) $(jobid) $(fname) \'{task}\' {eosdir} {self.opts.wdir} {gt}"',
                                      '-append', f'+JobFlavour = "{self.opts.resubflv}"'],
                                     capture_output=True)
                #remove('args.txt')
                if ret.returncode == 0:
                    # awfully parse condor_submit output to get the clusterId
                    self.log.info(ret.stdout.decode().strip())
                    cluster = ret.stdout.decode().strip().split()[-1][:-1]
                    logurl = 'https://ecallogs.web.cern.ch/'
                    # set the job status to idle and update the htc-id and log file URL
                    for i, jid in enumerate(failedjobs):
                        jctrl.idle(jid=jid, fields={'htc-id': f"{cluster}.{i}", 'log': f'{logurl}/{self.task}-{cluster}-{i}.log'})
                else:
                    self.log.info("failed to submit jobs to condor. Error msg:"+ret.stderr.decode())
                    return -1
        return 0

if __name__ == '__main__':
    handler = PSValidationHandler(task='pulse-shapes-hlt',
                                  deps_tasks=['pulse-shapes-merge-fill'],
                                  dsetname=['/HLTPhysics/*/RAW', '/HIHLTPhysics/*/RAW'])

    ret = handler()
    
    sys.exit(ret)

get_opts = AutoCtrlScriptBase.export_options(PSValidationHandler)
