"""Plotting Tools: plot invariant mass distribution from main ECALElf ntuples"""

from typing import List, Dict, Optional, Union
import json
import pandas as pd
import numpy as np
import awkward as ak
import uproot
import mplhep as hep
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import logging
import datetime
from ecalautoctrl.CMSTools import QueryOMS
from omsapi import OMSAPI
from condiovtools import get_tag, get_cond_iov_runs, get_run_info, merge_iovruns
plt.style.use(hep.style.CMS)

def truncated_mean(arr):
    """
    Compute the mean of the invariant mass distribution restricting to 2 sigma
    """
    
    # compute mean and standard deviation for each time bin
    bin_size = ak.num(arr.invMass_ECAL_ele)[0]
    stds = np.repeat(np.std(arr.invMass_ECAL_ele.to_numpy(), axis=1, keepdims=True),
                     bin_size, axis=1)
    mean = np.repeat(np.mean(arr.invMass_ECAL_ele.to_numpy(), axis=1, keepdims=True),
                     bin_size, axis=1)
    
    # truncate data within 2 std from the mean by masking everything outside the window
    mass = arr.invMass_ECAL_ele.to_numpy()
    mass.mask = np.abs(arr.invMass_ECAL_ele-mean) > 2*stds
    
    return np.mean(mass, axis=1), np.std(mass, axis=1)/np.sqrt(ak.num(arr.invMass_ECAL_ele))

def com_from_year(year):
    """
    pp centre-of-mass energy for a year.

    :param year: year.
    :return com for given year.
    """
    if year < 2012:
        return 7.
    elif year < 2015:
        return 8.
    elif year < 2022:
        return 13.
    else:
        return 13.6

class NumpyArrayEncoder(json.JSONEncoder):
    """Helper class to encode NumPy arrays into json"""
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)

class ZeeMonPlots:
    """
    Produce scale stability and invariant mass distribution plots.

    :param files: input files.
    :param categories: dictionary defining the categories:
    { "name" : {"expr": function, "color": plot color}}
    :param lumi_file: luminosity data from brilcalc.
    :param gt: Global tag to load IOV changes.
    :param load_data: if not false read data from pre-processed file.
    :param minutes: lenght in minutes of the luminosity bins.
    :param events_per_bin: number of events per bin.
    """

    def __init__(self,
                 files: List[str] = None,
                 categories: Dict[str, Dict] = None,
                 lumi_file: str = None,
                 gt: str = '',
                 load_data: Optional[Union[bool, Union[str, List[str]]]] = False,
                 minutes: Optional[float] = 600.,
                 events_per_bin: Optional[int] = 5000):
        self.gt = gt
        self.data = {}
        self.colors = {k: v['color'] for k, v in categories.items()}
        self.alphas = {k: v['alpha'] if 'alpha' in v.keys() else 1. for k, v in categories.items()}

        # read back data from json files
        if load_data:
            self.load_histograms(load_data)
        else:
            if not (files and categories and lumi_file):
                logging.warning('No data provided')
            # load the mee data
            evts = uproot.lazy({f: 'selected' for f in files},
                               filter_name=['invMass_ECAL_ele',
                                            'etaSCEle',
                                            'eventTime',
                                            'runNumber'])
            evts = evts[evts.invMass_ECAL_ele > 0]
            # split in categories
            self.data['mee'] = {}
            for cat, m in categories.items():
                sel = m['expr'](evts)
                idx = ak.argsort(evts.eventTime[sel])
                m['data'] = ak.unflatten(evts[sel][idx], counts=min(events_per_bin, len(evts[sel][idx])))
                mass, mass_err = truncated_mean(m['data'])
                self.data['mee'][cat] = {
                    'mass': mass,
                    'mass_err': mass_err,
                    'time': np.mean(m['data'].eventTime.to_numpy(), axis=1)}
            # load the luminosity data
            self.load_brilcalc_data(lumi_file=lumi_file, mins=minutes)

    def load_brilcalc_data(self, lumi_file: str, mins: float = 600.):
        """
        Load luminosity from files.

        :param lumi_file: luminosity data from brilcalc.
        :param mins: time bins duration.
        :return: pandas group splitted in bins of `mins` duration.
        """
        df = pd.read_csv(lumi_file, usecols=["run", "fill", "time", "delivered(/ub)"])
        lumi_bins = df.groupby(pd.cut(df['time'],
                                      bins=int(np.ceil((df['time'].max()-df['time'].min())/mins))))
        sel = ~np.isnan(lumi_bins.mean()['time'].values)
        self.data['lumi'] = {
            'time': lumi_bins.mean()['time'].values[sel],
            'inst_lumi': lumi_bins.mean()['delivered(/ub)'].values[sel]/10000/23,
            'int_lumi': lumi_bins.sum()['delivered(/ub)'].values[sel]/1e9}

    def load_histograms(self, json_files: Union[str, List[str]]):
        """
        Load luminosity and mee data from preprocessed file.

        :param json_file: json file names.
        """
        load = lambda x: np.array(x) if isinstance(x, list) else {k: load(v) for k, v in x.items()}
        concat = lambda x, y: {k : np.concatenate([x.get(k, []), y.get(k, [])]) for k in set(x.keys()).union(y.keys())}
        for jf in json_files:
            with open(jf) as jd:
                jdata = load(json.load(jd))
                if 'mee' not in jdata or 'lumi' not in jdata:
                    logging.warning(f'mee or lumi data missing from provided histogram file. Skipping {jf}')
                    continue
                else:
                    logging.info(f'loading: {jf}')
                # duplicate last time bin to smooth out inst lumi plots
                # (essentially setting the inst lumi to zero to avoid the
                #  having the bin in between fills show a constant pedestal
                # add a bin 1 minute after the last one
                jdata['lumi']['time'] = np.append(jdata['lumi']['time'], jdata['lumi']['time'][-1]+60)
                jdata['lumi']['time'] = np.insert(jdata['lumi']['time'], obj=0, values=jdata['lumi']['time'][0]-60)
                # set inst lumi to zero
                jdata['lumi']['inst_lumi'] = np.append(jdata['lumi']['inst_lumi'], 0)
                jdata['lumi']['inst_lumi'] = np.insert(jdata['lumi']['inst_lumi'], obj=0, values=0)
                # duplicate int lumi
                jdata['lumi']['int_lumi'] = np.append(jdata['lumi']['int_lumi'], jdata['lumi']['int_lumi'][-1])
                jdata['lumi']['int_lumi'] = np.insert(jdata['lumi']['int_lumi'], obj=0, values=jdata['lumi']['int_lumi'][0])
                if not self.data:
                    self.data = jdata
                else:
                    # assuming a certain data structure
                    # mee -> categories -> {'mass', 'mass_err', 'time'}
                    for k in set(self.data['mee'].keys()).union(jdata['mee'].keys()):
                        self.data['mee'][k] = concat(self.data['mee'].get(k, {}), jdata['mee'].get(k, {}))
                    # lumi -> {'int_lumi', 'inst_lumi', 'time'}
                    self.data['lumi'] = concat(self.data['lumi'], jdata['lumi'])

    def save_histograms(self, json_file: str):
        """
        Save processed histograms.

        :param json_file: output json file name.
        """
        with open(json_file, "w") as outfile:
            json.dump(self.data, outfile, cls=NumpyArrayEncoder, indent=4)

    def scale_monitoring(self, fmt='-', drawiovs=True, drawhist=False, years=[2022, 2023, 2024, 2025], last=None):
        """
        Produce the ECAL energy scale monitoring plots using the Zee invariant mass distribution.

        :return: matplotlib figure or None if not data are found to plot.
        """
        fig = plt.figure()
        if drawhist:
            gs = fig.add_gridspec(2, 2, wspace=0, hspace=0, width_ratios=[6,1], height_ratios=[2,1])
            axs = []
            axs.append(fig.add_subplot(gs[0, 0]))
            axs.append(fig.add_subplot(gs[1, 0], sharex=axs[0]))
            axs.append(fig.add_subplot(gs[0, 1], sharey=axs[0]))
            axs[2].tick_params(axis="y", labelleft=False)
        else:
            gs = fig.add_gridspec(2, hspace=0, height_ratios=[2,1])
            axs = gs.subplots(sharex=True)

        ymin = 85.
        ymax = 95.
        now = datetime.datetime.now()
        # mee graphs
        for cat, m in self.data['mee'].items():
            idxall = np.argsort(m['time'])
            idx = []
            # only plot selected years
            for i in idxall:
                dtime = m['time'][i].astype(dtype='datetime64[s]')
                for yr in years:
                    if pd.to_datetime(dtime).year == yr:
                        if last == None:
                            idx.append(i)
                        else:
                            # plot only the last x days
                            timediff = now - pd.to_datetime(dtime)
                            if timediff.days <= last:
                                idx.append(i)
            if len(idx) == 0:
                continue
            # plot mee
            mean = np.mean(m["mass"][idx[0]:idx[-1]+1])
            std = np.std(m["mass"][idx[0]:idx[-1]+1])
            axs[0].errorbar(x=m['time'][idx].astype(dtype='datetime64[s]'),
                            y=m['mass'][idx], 
                            yerr=m['mass_err'][idx],
                            color=self.colors[cat],
                            fmt=fmt,
                            label='$\\bf{'+cat+'}$: '+f'mean={mean:.2f}, s.d.={std:.2f}')
            if last != None:
                axs[0].text(0.7, 0.05, f'Last {last} days', transform=axs[0].transAxes, size='small')
            # plot mee histo
            if drawhist:
                nbins = 200.
                bins = np.arange(ymin, ymax, (ymax - ymin) / nbins)
                axs[2].hist(m['mass'][idx], bins=bins, orientation='horizontal', density=True, color=self.colors[cat], alpha=self.alphas[cat])

        # lumi
        lumi = self.data['lumi']
        idxall = np.argsort(lumi['time'])
        idx = []
        # only plot selected years
        for i in idxall:
            dtime = lumi['time'][i].astype(dtype='datetime64[s]')
            for yr in years:
                if pd.to_datetime(dtime).year == yr:
                    if last == None:
                        idx.append(i)
                    else:
                        timediff = now - pd.to_datetime(dtime)
                        if timediff.days <= last:
                            idx.append(i)
        if len(idx) == 0:
            return None

        # int lumi histo
        axs_int = axs[1].twinx()
        axs_int.hist(lumi['time'][idx].astype(dtype='datetime64[s]'),
                     weights=lumi['int_lumi'][idx],
                     bins=len(lumi['time'][idx]),
                     histtype='stepfilled', cumulative=True,
                     color='yellow', alpha=0.3)
        # inst lumi histo
        axs[1].hist(lumi['time'][idx].astype(dtype='datetime64[s]'),
                    weights=lumi['inst_lumi'][idx],
                    bins=np.append(lumi['time'][idx],
                                   lumi['time'][idx][-1]+60).astype(dtype='datetime64[s]'))

        dateformat = '%d/%m/%Y %H:%M'
        if (lumi['time'][idx[-1]] - lumi['time'][idx[0]]) > 86400.:  # more than one day
            dateformat = '%d/%m/%Y'
        plt.gca().xaxis.set_major_formatter(mdates.DateFormatter(dateformat))
        plt.gcf().autofmt_xdate()
        axs[0].set_ylabel('$m_{ee} (GeV)$')
        axs[0].grid()
        axs[0].legend()
        axs[0].set_ylim(ymin, ymax)
        axs[1].set_ylabel('$Inst. Lumi.$ \n $(10^{34} cm^{-2} s^{-1})$')
        axs[1].set_xlabel('Date')
        axs[1].set_ylim(0,2.25)
        axs[1].grid()
        if drawhist:
            axs[2].grid()
        axs_int.set_ylabel('$Int. Lumi. (fb^{-1})$')

        if drawiovs:
            # conditions update lines
            omsapi = OMSAPI("https://cmsoms.cern.ch/agg/api", "v1", cert_verify=False, verbose=False)
            omsapi.auth_oidc('ecalgit-omsapi', 'KXbuy4vxiETBc5C7FwteQxAF3X1irilx')
            omsquery = QueryOMS()

            timingRcdLabel = 'CC'
            # tags for which IOV updates can be shown in the plot
            psiovruns = get_cond_iov_runs(get_tag(self.gt, "EcalPulseShapesRcd"), "EcalCond")
            iciovruns = get_cond_iov_runs(get_tag(self.gt, "EcalIntercalibConstantsRcd"), "EcalCond")
            #timeiovruns = get_cond_iov_runs(get_tag(self.gt, "EcalTimeCalibConstantsRcd", timingRcdLabel), "EcalCond")
            ebaligniovruns = get_cond_iov_runs(get_tag(self.gt, "EBAlignmentRcd"), "Alignments")
            eealigniovruns = get_cond_iov_runs(get_tag(self.gt, "EEAlignmentRcd"), "Alignments")
            esaligniovruns = get_cond_iov_runs(get_tag(self.gt, "ESAlignmentRcd"), "Alignments")
            #pediovruns = get_cond_iov_runs(get_tag(self.gt, "EcalPedestalsRcd"), "EcalCond")

            # tags for which IOV updates are shown in the plot
            tagiovs = {"PS":psiovruns,
                       #"T_{" + timingRcdLabel + "}":timeiovruns,
                       "IC":iciovruns,
                       "A^{EB}":ebaligniovruns,
                       "A^{EE}":eealigniovruns,
                       "A^{ES}":esaligniovruns,
                       #"P":pediovruns
                      }

            runcondstrs = merge_iovruns(tagiovs)
            iovruns = [r for r in sorted(runcondstrs) if r > 347000]
            runstrs, run_start_times = get_run_info(iovruns, omsquery, omsapi)
            condstrs = ["{0}-{1}".format(runcondstrs[int(r)], r) for r in runstrs]

            xmin, xmax = axs[0].get_xlim()
            prev_runtime = mdates.date2num(0.)
            for condstr, run_start_time in zip(condstrs, run_start_times):
                runtime = np.datetime64(run_start_time, 's')
                if mdates.date2num(runtime) < xmin or mdates.date2num(runtime) > xmax:
                    continue
                axs[0].axvline(x=runtime, color='k', ls='--', lw=1)
                axs[1].axvline(x=runtime, color='k', ls='--', lw=1)
                halign = 'right'
                if mdates.date2num(runtime) - mdates.date2num(prev_runtime) < (xmax - xmin) / 80.:
                    halign = 'left'
                axs[1].text(runtime, 0.05, condstr, horizontalalignment=halign, verticalalignment='bottom', rotation='vertical', size=8.)
                prev_runtime = runtime

        plt.axes(axs[0])

        # plot decorations
        from_year = pd.to_datetime(lumi['time'][idx[0]].astype(dtype='datetime64[s]')).year
        to_year = pd.to_datetime(lumi['time'][idx[-1]].astype(dtype='datetime64[s]')).year
        if from_year == to_year:
            year = from_year
        else:
            year = None  # TODO instead set the year range manually in the label

        if com_from_year(from_year) == com_from_year(to_year):
            com = com_from_year(from_year)
            int_lumi = sum(lumi['int_lumi'][idx[0]:idx[-1]+1])
        else:
            com = None  # TODO instead set the com range manually in the label
            int_lumi = None

        hep.cms.label("Internal", data=True, year=year, com=com, lumi=int_lumi, lumi_format="{0:.1f}")

        return fig
