#!/usr/bin/env python3
import sys
import os 
import itertools
import pickle
import matplotlib.pyplot as plt
import datetime
from typing import Optional, List
from ecalautoctrl import JobCtrl, prev_task_data_source, process_by_fill
from ecalautoctrl.TaskHandlers import AutoCtrlScriptBase

from zeemon_tools import ZeeMonPlots
from runjobs import ZeeMonHandler


class ZeeMonCmlHandler(ZeeMonHandler):
    """
    Execute all the steps to generate the Zee cumulative monitoring plots.

    :param task: task name.
    :param prev_input: name of the task from which gather the input data.
    :param deps_tasks: list of workfow dependencies.
    """

    def __init__(self,
                 task: str,
                 prev_input: str,
                 deps_tasks: Optional[List[str]] = None,
                 **kwargs):
        super().__init__(task=task,
                         prev_input=prev_input,
                         deps_tasks=deps_tasks,
                         **kwargs)

    def process_cml(self, files: List[str] = None, gt: str = ''):
        """
        Load preprocessed histograms and produce the cumulative plots.

        :param files: list of histogram files.
        :gt: GT for plotting IOVs
        """
        # Initialize the plotting tool
        # extract only the main ntuples files (it's always the last one)
        plots = ZeeMonPlots(files=files,
                            categories={
                                'EB': {'color': 'darkslateblue', 'alpha': 1.},
                                'EE': {'color': 'steelblue', 'alpha': 0.7}
                            },
                            load_data=files,
                            gt=gt)

        path = os.path.abspath(f'{self.opts.eosplots}')
        os.makedirs(path, exist_ok=True)

        # get the plots
        years = [2022, 2023, 2024, 2025]
        years = sorted(years)
        # yearly plots
        yrmin = years[-1]
        yrmax = years[0]
        for yr in years:
            fig = plots.scale_monitoring(fmt='.', drawhist=True, years=[yr])
            if fig == None:
                continue
            if yr < yrmin:
                yrmin = yr
            if yr > yrmax:
                yrmax = yr
            fig.savefig(f'{path}/mee_scale_{self.campaign}_{yr}.png')
            with open(f'{path}/mee_scale_{self.campaign}_{yr}.pkl', 'wb') as fpkl:
                pickle.dump(fig, fpkl)
            plt.close(fig)

        # plot last 2 and 4 weeks for prompt campaign
        thisyear = datetime.datetime.now().year
        if 'prompt/zeemon' in path and thisyear in years:
            last = 28
            fig = plots.scale_monitoring(fmt='.', drawhist=True, years=[thisyear], last=last)
            if fig != None:
                fig.savefig(f'{path}/mee_scale_last{last}days.png')
                with open(f'{path}/mee_scale_last{last}days.pkl', 'wb') as fpkl:
                    pickle.dump(fig, fpkl)
                plt.close(fig)

            last = 14
            fig = plots.scale_monitoring(fmt='.', drawhist=True, years=[thisyear], last=last)
            if fig != None:
                fig.savefig(f'{path}/mee_scale_last{last}days.png')
                with open(f'{path}/mee_scale_last{last}days.pkl', 'wb') as fpkl:
                    pickle.dump(fig, fpkl)
                plt.close(fig)

        # overall plot
        if yrmin != yrmax and yrmax > yrmin:
            fig = plots.scale_monitoring(fmt='.', drawhist=True, years=years)
            if fig != None:
                fig.savefig(f'{path}/mee_scale_{self.campaign}_{yrmin}_to_{yrmax}.png')
                with open(f'{path}/mee_scale_{self.campaign}_{yrmin}_to_{yrmax}.pkl', 'wb') as fpkl:
                    pickle.dump(fig, fpkl)
                plt.close(fig)

        # save processed data
        jsonpath = f'{path}/histograms_{self.campaign}.json'
        plots.save_histograms(jsonpath)

        return jsonpath, self.opts.plotsurl

    def resubmit(self) -> int:
        """
        Resubmit failed runs.

        :return: status.
        """
        # get the new run to process
        runs = self.rctrl.getRuns(status={self.task: 'processing'})
        # check if any job failed, if so resubmit
        for run in runs:
            jctrl = JobCtrl(task=self.task,
                            campaign=self.campaign,
                            tags={'run_number': run['run_number'],
                                  'fill': run['fill']},
                            dbname=self.opts.dbname)
            if jctrl.taskExist() and len(jctrl.getFailed()) > 0:
                files = jctrl.getJob(jid=0, last=True)[-1]['inputs']
                try:
                    jctrl.running(jid=0)
                    self.log.info(f'Resubmitting processing for run: {run["run_number"]}')
                    ret = self.process_cml(files=files.split(','), gt=run['globaltag'])
                    if not ret:
                        jctrl.failed(jid=0)
                    else:
                        # mark as completed
                        jctrl.done(jid=0, fields={
                            'output': ret[0],
                            'plots': ret[1]})

                except Exception as e:
                    jctrl.failed(jid=0)
                    self.log.error(f'Failed producing the cumulative monitoring plot for fill {run["fill"]}: {e}')
                    continue

    def submit(self) -> int:
        """
        Submit new runs.

        :return: status.
        """
        # the cumulative plot is produced using always all available data
        # hence we can merge multiple fills together
        group = list(itertools.chain.from_iterable(self.groups()))
        all_runs = self.rctrl.getRuns(status={self.prev_input: 'done'}, active=False)
        all_runs += self.rctrl.getRuns(status={self.prev_input: 'done'}, active=True)
        fdict = sorted(self.get_files(all_runs))

        if group and fdict is not None and len(fdict) > 0:
            # master run
            run = group[-1]
            jctrl = JobCtrl(task=self.task,
                            campaign=self.campaign,
                            tags={'run_number': run['run_number'], 'fill': run['fill']},
                            dbname=self.opts.dbname)

            repr = (run[self.task] == 'reprocess')
            if not jctrl.taskExist() or repr:
                jctrl.createTask(jids=[0],
                                 recreate=repr,
                                 fields=[{
                                     'group': ','.join([r['run_number'] for r in group[:-1]]),
                                     'inputs': ','.join(fdict)}])
            try:
                jctrl.running(jid=0)
                self.rctrl.updateStatus(run=run['run_number'], status={self.task: 'processing'})
                for r in group[:-1]:
                    self.rctrl.updateStatus(run=r['run_number'], status={self.task: 'merged'})
                self.log.info(f'Processing cumulative plots using {len(fdict)} fills up to fill {run["fill"]}')
                ret = self.process_cml(files=fdict, gt=run['globaltag'])
                if not ret:
                    jctrl.failed(jid=0)
                else:
                    # mark as completed
                    jctrl.done(jid=0, fields={
                        'output': ret[0],
                        'plots': ret[1]})

            except Exception as e:
                jctrl.failed(jid=0)
                self.log.error(f'Failed producing the cumulative monitoring plots up to fill {run["fill"]}: {e}')


if __name__ == '__main__':
    handler = ZeeMonCmlHandler(task='zee-mon-cml',
                               deps_tasks=['zee-mon'],
                               prev_input='zee-mon')

    ret = handler()

    sys.exit(ret)

get_opts = AutoCtrlScriptBase.export_options(ZeeMonCmlHandler)
 
