#!/usr/bin/env python3
import sys
import os
import subprocess
import matplotlib.pyplot as plt
import numpy as np
from typing import Optional, List, Tuple
from ecalautoctrl import JobCtrl, HandlerBase, prev_task_data_source, process_by_fill
from ecalautoctrl.TaskHandlers import AutoCtrlScriptBase

from zeemon_tools import ZeeMonPlots

@prev_task_data_source
@process_by_fill(fill_complete=True)
class ZeeMonHandler(HandlerBase):
    """
    Execute all the steps to generate the Zee monitoring plots.
    Process fills that have been dumped (completed).

    :param task: task name.
    :param prev_input: name of the task from which gather the input data.
    :param deps_tasks: list of workfow dependencies.
    """

    def __init__(self,
                 task: str,
                 prev_input: str,
                 deps_tasks: Optional[List[str]]=None,
                 **kwargs):
        super().__init__(task=task, deps_tasks=deps_tasks, **kwargs)
        self.prev_input = prev_input

        self.submit_parser.add_argument('--eosplots',
                                        dest='eosplots',
                                        default=None,
                                        type=str,
                                        help='Plots webpage EOS path')
        self.submit_parser.add_argument('--plotsurl',
                                        dest='plotsurl',
                                        default=None,
                                        type=str,
                                        help='Plots webpage url')

        self.resubmit_parser.add_argument('--eosplots',
                                          dest='eosplots',
                                          default=None,
                                          type=str,
                                          help='Plots webpage EOS path')
        self.resubmit_parser.add_argument('--plotsurl',
                                          dest='plotsurl',
                                          default=None,
                                          type=str,
                                          help='Plots webpage url')

    def process_fill(self, fill: int, files: List[str], gt: str) -> Tuple:
        """
        Process data by fill and produce the Zee peak monitoring plots.

        :return: output, plotsurl.
        """

        # get brilcalc data
        brilcalc_file = f'lumi_data_{fill}.csv'
        runs=self.rctrl.getRunsInFill(fill)
        runMin=min(runs)
        runMax=max(runs)
        cmd = f'brilcalc lumi --tssec --output-style csv --begin {runMin} --end {runMax}  --byls | tail -n +2 | head -n -3 | sed "s/:/,/g" | sed "s/ls/ls,ls/g" | sed "s/#//g" > {brilcalc_file}'
        self.log.info(f'Getting data from brilcalc: {cmd}')
        ret = subprocess.run(cmd, shell=True, stderr=subprocess.STDOUT)
        ret.check_returncode()
        # Initialize the plotting tool
        # extract only the main ntuples files (it's always the last one)
        plots = ZeeMonPlots(files=files,
                            categories = {
                                'EB': {'expr': lambda arr: np.max(np.abs(arr.etaSCEle[:,:-1]), axis=1)<1.5, 
                                        'color': 'darkslateblue'},
                                'EE': {'expr': lambda arr: (np.min(np.abs(arr.etaSCEle[:,:-1]), axis=1)>1.5) * (np.max(np.abs(arr.etaSCEle[:,:-1]), axis=1)<2.5), 
                                        'color': 'steelblue'}
                            },
                            lumi_file=brilcalc_file,
                            gt=gt)

        path = os.path.abspath(f'{self.opts.eosplots}/{fill}/')
        os.makedirs(path, exist_ok=True)
        
        # get the plot
        fig = plots.scale_monitoring()
        if fig == None:
            return False
        fig.savefig(f'{path}/mee_scale.png')
        plt.close(fig)

        # save processed data
        plots.save_histograms(f'{path}/histograms.json')

        return f'{path}/histograms.json', f'{self.opts.plotsurl}/{fill}/'

    def resubmit(self) -> int:
        """
        Resubmit failed runs.

        :return: status.
        """

        # get the new run to process
        runs = self.rctrl.getRuns(status = {self.task: 'processing'})
        # check if any job failed, if so resubmit
        for run in runs:
            jctrl = JobCtrl(task=self.task,
                            campaign=self.campaign,
                            tags={'run_number': run['run_number'],
                                  'fill': run['fill']},
                            dbname=self.opts.dbname)
            if jctrl.taskExist() and len(jctrl.getFailed())>0:
                files = jctrl.getJob(jid=0, last=True)[-1]['inputs'] 
                #try:
                jctrl.running(jid=0)
                self.log.info(f'Resubmitting processing for fill: {run["fill"]}')
                ret = self.process_fill(fill=run['fill'], files=files.split(','), gt=run['globaltag'])
                if not ret:
                    jctrl.failed(jid=0)
                else:
                    # mark as completed
                    jctrl.done(jid=0, fields={
                        'output': ret[0],
                        'plots': ret[1]})

                # except Exception as e:
                #     jctrl.failed(jid=0)
                #     self.log.error(f'Failed producing the monitoring plots for fill {run["fill"]}: {e}')
                #     continue

    def submit(self) -> int:
        """
        Submit new runs.

        :return: status.
        """
        for group in self.groups():
            # master run
            run = group[-1]
            fdict = self.get_files(group)
            if fdict is not None and len(fdict)>0:
                jctrl = JobCtrl(task=self.task,
                                campaign=self.campaign,
                                tags={'run_number': run['run_number'], 'fill': run['fill']},
                                dbname=self.opts.dbname)

                repr = (run[self.task] == 'reprocess')
                if not jctrl.taskExist() or repr:
                    jctrl.createTask(jids=[0],
                                     recreate=repr,
                                     fields=[{
                                         'group': ','.join([r['run_number'] for r in group[:-1]]),
                                         'inputs': ','.join([f.split(',')[-1] for f in fdict])}])
                try:
                    jctrl.running(jid=0)
                    self.rctrl.updateStatus(run=run['run_number'], status={self.task: 'processing'})
                    for r in group[:-1]:
                        self.rctrl.updateStatus(run=r['run_number'], status={self.task: 'merged'})
                    self.log.info(f'Processing fill: {run["fill"]}')
                    ret = self.process_fill(fill=run['fill'], files=[f.split(',')[-1] for f in fdict], gt=run['globaltag'])
                    if not ret:
                        jctrl.failed(jid=0)
                    else:
                        # mark as completed
                        jctrl.done(jid=0, fields={
                            'output': ret[0],
                            'plots': ret[1]})

                except Exception as e:
                    jctrl.failed(jid=0)
                    self.log.error(f'Failed producing the monitoring plots for fill {run["fill"]}: {e}')
                    continue


if __name__ == '__main__':
    handler = ZeeMonHandler(task='zee-mon',
                            deps_tasks=['ecalelf-ntuples-zskim'],
                            prev_input='ecalelf-ntuples-zskim')

    ret = handler()

    sys.exit(ret)

get_opts = AutoCtrlScriptBase.export_options(ZeeMonHandler)

