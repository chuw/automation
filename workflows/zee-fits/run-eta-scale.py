#!/usr/bin/env python3
import sys
import os
import shutil
import subprocess
from argparse import Namespace
import matplotlib.pyplot as plt
import numpy as np
import json
from typing import Optional, Dict, List
from ijazz import Config
from ijazz.bin.IJazZ import main as ijazz_main
from ijazz.bin.IJazZHDF import display_hdf as ijazz_plots
from ijazz.bin.IJazZEtaScale import eta_scale as ijazz_etascale
from ecalautoctrl import JobCtrl, HandlerBase, prev_task_data_source, process_by_intlumi,process_by_run
from ecalautoctrl.TaskHandlers import AutoCtrlScriptBase


target_lumi = 2000

@prev_task_data_source
@process_by_intlumi(target=target_lumi)
class IJazZHandler(HandlerBase):
    """
    Execute all the steps to generate the Zee monitoring plots.
    Process fills that have been dumped (completed).

    :param task: task name.
    :param prev_input: name of the task from which gather the input data.
    :param deps_tasks: list of workfow dependencies.
    """

    def __init__(self,
                 task: str,
                 prev_input: str,
                 deps_tasks: Optional[List[str]]=None,
                 **kwargs):
        super().__init__(task=task, deps_tasks=deps_tasks, **kwargs)
        self.prev_input = prev_input

        self.submit_parser.add_argument('--config',
                                        dest='config',
                                        default=None,
                                        type=str,
                                        required=True,
                                        help='Mandatory IJazZ config file')
        self.submit_parser.add_argument('--eosplots',
                                        dest='eosplots',
                                        default=None,
                                        type=str,
                                        help='Plots webpage EOS path')
        self.submit_parser.add_argument('--plotsurl',
                                        dest='plotsurl',
                                        default=None,
                                        type=str,
                                        help='Plots webpage url')
        self.submit_parser.add_argument('--ref_file_cfg',
                                        dest='ref_file_cfg',
                                        default='config/ijazz_ref_file.json',
                                        type=str,
                                        help='ijazz MC reference file config')
        
        self.submit_parser.add_argument('--ref_file_path',
                                        dest='ref_file_path',
                                        default=None,
                                        type=str,
                                        help='ijazz MC reference files path (directory)')

        
        self.resubmit_parser.add_argument('--config',
                                        dest='config',
                                        default=None,
                                        type=str,
                                        required=True,
                                        help='Mandatory IJazZ config file')

        self.resubmit_parser.add_argument('--eosplots',
                                          dest='eosplots',
                                          default=None,
                                          type=str,
                                          help='Plots webpage EOS path')
        self.resubmit_parser.add_argument('--plotsurl',
                                          dest='plotsurl',
                                          default=None,
                                          type=str,
                                          help='Plots webpage url')

        self.resubmit_parser.add_argument('--ref_file_cfg',
                                        dest='ref_file_cfg',
                                        default='config/ijazz_ref_file.json',
                                        type=str,
                                        help='configuration of ijazz MC reference file')
        
        self.resubmit_parser.add_argument('--ref_file_path',
                                        dest='ref_file_path',
                                        default=None,
                                        type=str,
                                        help='ijazz MC reference files path (directory)')

    def select_ref_file(self, run_number):
        """ Select reference file from .json"""

        with open(self.opts.ref_file_cfg, 'r') as f:
            ref_files = json.load(f)

        
        default_rfile = ""
        for rfile in ref_files["ref_files"]:
            if int(run_number) <= int(rfile["lastrun"]) :
                return self.opts.ref_file_path+rfile["filepath"]
            if rfile["name"]=="default" :
                default_rfile = rfile["filepath"]
                
        return self.opts.ref_file_path+default_rfile        
            
    def etascale_args(self,config_json_, outdir_, lumi_):
        """
        Prepare configuration for eta_scale calculation
        """
        ns= Namespace(config=config_json_,out=outdir_,lumi=lumi_,interactive=False)
        return ns

    def etascale_json(self,name,hd5path,refpath):
        """
        Prepare json for etascale calculation
        """

        dict = {}
        key = 'master_'+name
        group ={}
        group['path']=hd5path
        group['name']=name
        group['lumi']=1
        dict[key]= group

        mcgroup={}
        mcgroup['path']=refpath
        mcgroup['name']='MC'
        mcgroup['lumi']=1
        dict['ref']=mcgroup

        return dict

    def process_data(self, groups: List[Dict]) -> int:
        """
        Process the eta scales for grouped runs

        :return: status.
        """

        main_cfg = Config(self.opts.config)
        main_cfg.use_absolute_path = True

        for group in groups:
            # main run
            run = group[-1]
            # ECALELF produces 4 output files, we only need the main one (the last one in the list)
            ecalelf_files = [lf.split(',')[-1] for lf in self.get_files(group)]
            if ecalelf_files is not None and len(ecalelf_files)>0:
                jctrl = JobCtrl(task=self.task,
                                campaign=self.campaign,
                                tags={'run_number': run['run_number'], 'fill': run['fill']},
                                dbname=self.opts.dbname)
                repr = (run[self.task] == 'reprocess')
                if not jctrl.taskExist() or repr:
                    jctrl.createTask(jids=[0],
                                     recreate=repr,
                                     fields=[{
                                         'group': ','.join([r['run_number'] for r in group[:-1]]),
                                         'inputs': ','.join([f.split(',')[-1] for f in ecalelf_files])}])
                try:
                    jctrl.running(jid=0)
                    intlumi = run['lumi']
                    self.rctrl.updateStatus(run=run['run_number'], status={self.task: 'processing'})
                    for r in group[:-1]:
                        self.rctrl.updateStatus(run=r['run_number'], status={self.task: 'merged'})
                        intlumi += r['lumi']

                    self.log.info(f'Processing runs {group[0]["run_number"]} to {run["run_number"]} ({intlumi}/pb). ')

                    # Run IJazZ routine
                    main_cfg.root_files = ecalelf_files
                    outpath_name = str(self.opts.eosdir)+'/'+str(run['run_number'])+'/'
                    outfile_name =outpath_name+'ijazz_results.hdf5'
                    main_cfg.hdf = os.path.abspath(outfile_name)
                    eosplots_dirname= self.opts.eosplots+'/'+str(run['run_number'])
                    os.makedirs(eosplots_dirname,exist_ok=True)

                    ijazz_main(main_cfg)
                    ijazz_plots(main_cfg.hdf,True,None,None, eosplots_dirname)

                    #second step: scale calculation
                    this_ref_file=self.select_ref_file(run['run_number'])
                    self.log.info(f'Using {this_ref_file} as reference')
                    cfg_json = self.etascale_json(run['run_number'], outfile_name, this_ref_file)
                    etascale_cfg = self.etascale_args(cfg_json, outpath_name, target_lumi)
                    ijazz_etascale(etascale_cfg)

                    #copy relevant plots for web display, brute force
                    plots_to_show = ['abs_scale_master_'+str(run['run_number'])+'.jpg',
                                     'eta_scale_master_'+str(run['run_number'])+'.jpg',
                                     'R9_correction.jpg',
                                     'abs_scale_ref.jpg']

                    for file in plots_to_show:
                        shutil.copy2(outpath_name+'/'+file,eosplots_dirname)

                    jctrl.done(jid=0, fields={
                        'output': str(outpath_name),
                        'plots': str(self.opts.plotsurl) +'/'+str(run['run_number'])})

                except Exception as e:
                    jctrl.failed(jid=0)
                    self.log.error(f'Failed running IJazZ with config {self.opts.config} on runs {group[0]["run_number"]} to {run["run_number"]}: {e}')

        return 0

    def submit(self):
        """
        Execute the IJazZ calculations.
        """

        # process new runs / fill
        self.process_data(groups=self.groups())

        return 0

    def resubmit(self):
        """
        Resubmit run ranges with failed jobs
        """

        # get runs in status processing
        runs = self.rctrl.getRuns(status = {self.task : 'processing'})

        # check if any job failed before resubmitting
        for run_dict in runs:
            jctrl = JobCtrl(task=self.task,
                            campaign=self.campaign,
                            tags={'run_number' : run_dict['run_number'],
                                  'fill' : run_dict['fill']},
                            dbname=self.opts.dbname)
            if jctrl.taskExist() and len(jctrl.getFailed()) > 0:
                self.process_data(groups=[[run_dict]])

        return 0

if __name__ == '__main__':
    handler = IJazZHandler(task='zee-eta-scale',
                           prev_input='ecalelf-ntuples-zskim',
                           deps_tasks=['ecalelf-ntuples-zskim'])

    ret = handler()

    sys.exit(ret)

get_opts = AutoCtrlScriptBase.export_options(IJazZHandler)
